package net.omnidrone.logic;

import java.util.UUID;

import com.google.protobuf.Any;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import net.omnidrone.game.protobuf.GameProtos;

public class Client {
	static final boolean SSL = System.getProperty("ssl") != null;
    static final String HOST = System.getProperty("host", "127.0.0.1");
    static final int PORT = Integer.parseInt(System.getProperty("port", "8081"));

    public static void main(String[] args) throws Exception {
        
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
             .channel(NioSocketChannel.class)
             .handler(new ClientInitializer());

            // Make a new connection.
            Channel ch = b.connect(HOST, PORT).sync().channel();

            // Get the handler instance to initiate the request.
            ClientHandler handler = ch.pipeline().get(ClientHandler.class);

            String userId = UUID.randomUUID().toString();
            // Request
            GameProtos.UpdateVelocity.Builder updateVelocity = GameProtos.UpdateVelocity.newBuilder().setVelocity(666);
            GameProtos.GameCommand.Builder builder = GameProtos.GameCommand.newBuilder();
            //builder.setCommandType(GameProtos.UPDATEVELOCITY_FIELD_NUMBER);
            builder.setUserId(userId);
            builder.setCommand(Any.pack(updateVelocity.build()));
            handler.sendSpaceshipCommand(builder.build());

//            GameProtos.UpdateCargo.Builder updateCargo = GameProtos.UpdateCargo.newBuilder().setCargo("some cargo").setName("falcon");
//            builder = GameProtos.GameCommand.newBuilder();
//            builder.setCommandType(GameProtos.UPDATECARGO_FIELD_NUMBER);
//            builder.setUserId(userId);
//            builder.setExtension(GameProtos.updateCargo, updateCargo.build());
//            handler.sendSpaceshipCommand(builder.build());
//
//            // Request and get the response.
//            builder = GameProtos.GameCommand.newBuilder();
//            builder.setCommandType(GameProtos.GETSPACESHIP_FIELD_NUMBER);
//            builder.setUserId(userId);
//            builder.setExtension(GameProtos.getSpaceship, GameProtos.GetSpaceship.newBuilder().build());
//            handler.sendSpaceshipCommand(builder.build());
//
//            GameProtos.GetSpaceshipResponse response = handler.getSpaceshipResponse();
//            System.err.println("spaceship received;");
//            System.err.println("name;" + response.getName());
//            System.err.println("velocity;" + response.getVelocity());
//            System.err.println("cargo;" + response.getCargo());
            
//            // Close the connection.
            ch.close();

        } finally {
            group.shutdownGracefully();
        }
    }
}
