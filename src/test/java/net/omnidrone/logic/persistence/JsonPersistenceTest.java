package net.omnidrone.logic.persistence;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.elasticsearch.common.collect.Sets;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TableStatus;
import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import net.omnidrone.logic.domain.Spaceship;
import net.omnidrone.logic.session.Database;
import net.omnidrone.logic.session.DatabaseObject;
import net.omnidrone.savage.commons.Pair;
import net.omnidrone.savage.config.EnvironmentProvider;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBConfiguration;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBLifecycle;

public class JsonPersistenceTest {

	static Injector injector;
	private static boolean setupDone = false;
	
	private static String TABLE_NAME = "jsonpersistence-test";
	private static String TABLE_NAME2 = "jsonpersistence-test2";

	@BeforeClass
	public static void init() {
		injector = Guice.createInjector(new AbstractModule() {

			@Override
			protected void configure() {

				DynamoDBConfiguration config = new DynamoDBConfiguration();
				config.accessKey = "AKIAIMGAFWX3NIUUWMBA";
				config.secretKey = "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";
				config.dynamoTestDb = false;
				config.regionName = "eu-west-1";
				bind(DynamoDBConfiguration.class).toInstance(config);
				bind(DynamoDBLifecycle.class);
				bind(JsonPersistence.class).to(DynamoDBJsonPersistence.class);
			}
		});
		
	}

	@Before
	public void cleanUp() {
		if (!setupDone) {
			final EnvironmentProvider envProvider = injector.getInstance(EnvironmentProvider.class);
			final DynamoDBLifecycle dynamoDBLifecycle = injector.getInstance(DynamoDBLifecycle.class);
			dynamoDBLifecycle.onStartUp();

			AmazonDynamoDB dynamoDB = dynamoDBLifecycle.getAmazonDynamoDB();
			
			try {
				dynamoDB.deleteTable(new DeleteTableRequest().withTableName(TABLE_NAME));
				waitForTableToBeDeleted(dynamoDB, TABLE_NAME);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				dynamoDB.createTable(createTable(TABLE_NAME, new Pair<String, String>(DynamoDBJsonPersistence.DYNAMO_TABLE_ID, "S"), null));
				waitForTableToBecomeAvailable(dynamoDB, TABLE_NAME);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				dynamoDB.createTable(createTable(TABLE_NAME2, new Pair<String, String>(DynamoDBJsonPersistence.DYNAMO_TABLE_ID, "S"), null));
				waitForTableToBecomeAvailable(dynamoDB, TABLE_NAME2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		setupDone = true;

	}


	@Test
	public void testNested() {
		Gson gson = new Gson();
		String id = UUID.randomUUID().toString().substring(0, 4);
		JsonPersistence jsonPersistence = injector.getInstance(JsonPersistence.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(DynamoDBJsonPersistence.DYNAMO_TABLE_ID, id);
		map.put("attr1", "value1");
		
		Map<String, Object> nestedMap = new HashMap<String, Object>();
		nestedMap.put("nestedAttribute", "nestedValue");
		nestedMap.put("nestedNumeric", 10);
		nestedMap.put("nestedList", Arrays.asList(1,2,3));
		map.put("nested", nestedMap);
		String jsonValue = gson.toJson(map);
		System.err.println("store value: " + jsonValue);
		//jsonPersistence.update(TABLE_NAME, id, map);
		jsonPersistence.update(TABLE_NAME, id, jsonValue);
		Map<String, Object> result = jsonPersistence.get(TABLE_NAME, id);
		for (Map.Entry<String, Object> entry : result.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		
		jsonPersistence.updateAttribute(TABLE_NAME, id, "attr1", "updatedValue");
		
		result = jsonPersistence.get(TABLE_NAME, id);
		for (Map.Entry<String, Object> entry : result.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		
		Map<String, Object> updateAttributes = new HashMap<String, Object>();
		updateAttributes.put("nested.nestedNumeric", 99);
		updateAttributes.put("nested.nestedAttributed", "updatedValue");
		jsonPersistence.updateAttributes(TABLE_NAME, id, updateAttributes);
		
		System.err.println("attr1 should still exist");
		result = jsonPersistence.get(TABLE_NAME, id);
		for (Map.Entry<String, Object> entry : result.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		
		jsonPersistence.deleteAttributes(TABLE_NAME, id, "attr1");
		System.err.println("attr deleted;");
		
		List<String> attributes = new ArrayList<String>();
		attributes.add("attr1");
		attributes.add("nested.nestedList");
		Map<String, Object> result2 = jsonPersistence.getAttributes(TABLE_NAME, id, attributes);
		for (Map.Entry<String, Object> entry : result2.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		
	}
	
	@Test
	public void testMultipleIds() {
		Gson gson = new Gson();
		String id1 = UUID.randomUUID().toString().substring(0, 4);
		String id2 = UUID.randomUUID().toString().substring(0, 4);
		JsonPersistence jsonPersistence = injector.getInstance(JsonPersistence.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("attr1", "value1");
		
		Map<String, Object> nestedMap = new HashMap<String, Object>();
		nestedMap.put("nestedAttribute", "nestedValue");
		nestedMap.put("nestedNumeric", 10);
		nestedMap.put("nestedList", Arrays.asList(1,2,3));
		map.put("nested", nestedMap);
		String jsonValue = gson.toJson(map);
		System.err.println("store value: " + jsonValue);
		jsonPersistence.update(TABLE_NAME, id1, jsonValue);
		String jsonValue2 = gson.toJson(map);
		System.err.println("store value: " + jsonValue2);
		jsonPersistence.update(TABLE_NAME, id2, jsonValue2);
		
		List<String> attributes = new ArrayList<String>();
		attributes.add("nested");
		Map<String, Object> result = jsonPersistence.getMultipleIds(new HashSet<String>(Arrays.asList(id1, id2)), TABLE_NAME, attributes);
		for (Map.Entry<String, Object> entry : result.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		Map<String, Object> resultAll = jsonPersistence.getMultipleIdsAll(new HashSet<String>(Arrays.asList(id1, id2)), TABLE_NAME);
		for (Map.Entry<String, Object> entry : resultAll.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
	}
	
	@Test
	public void testMultipleTables() {
		Gson gson = new Gson();
		String id1 = UUID.randomUUID().toString().substring(0, 4);
		String id2 = UUID.randomUUID().toString().substring(0, 4);
		JsonPersistence jsonPersistence = injector.getInstance(JsonPersistence.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("attr1", "value1");
		
		Map<String, Object> nestedMap = new HashMap<String, Object>();
		nestedMap.put("nestedAttribute", "nestedValue");
		nestedMap.put("nestedNumeric", 10);
		nestedMap.put("nestedList", Arrays.asList(1,2,3));
		map.put("nested", nestedMap);
		String jsonValue = gson.toJson(map);
		System.err.println("store value: " + jsonValue);
		jsonPersistence.update(TABLE_NAME, id1, jsonValue);
		
		String jsonValue2 = gson.toJson(map);
		System.err.println("store value: " + jsonValue2);
		jsonPersistence.update(TABLE_NAME, id2, jsonValue2);
		
		map = new HashMap<String, Object>();
		map.put("attr10", "value10");
		jsonValue = gson.toJson(map);
		System.err.println("store value: " + jsonValue);
		jsonPersistence.update(TABLE_NAME2, id1, jsonValue);
		
		map = new HashMap<String, Object>();
		map.put("attr11", "value11");
		jsonValue = gson.toJson(map);
		System.err.println("store value: " + jsonValue);
		jsonPersistence.update(TABLE_NAME2, id2, jsonValue);
		
		List<String> attributes1 = new ArrayList<String>();
		attributes1.add("nested");
		attributes1.add("attr1");
		List<String> attributes2 = new ArrayList<String>();
		attributes2.add("attr10");
		attributes2.add("attr11");
		Map<String, List<String>> tableAttributes = new HashMap<String, List<String>>();
		tableAttributes.put(TABLE_NAME, attributes1);
		tableAttributes.put(TABLE_NAME2, attributes2);
		Map<String, Object> result1 = jsonPersistence.getFromMultipleTables(id1, tableAttributes);
		System.err.println("multiple tables:");
		for (Map.Entry<String, Object> entry : result1.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		Map<String, Object> result1All = jsonPersistence.getFromMultipleTablesAll(id1, new ArrayList<String>(tableAttributes.keySet()));
		System.err.println("multiple tables all:");
		for (Map.Entry<String, Object> entry : result1All.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		Map<String, Map<String, Object>> result2 = jsonPersistence.getForMultipleIdAndMultipleTables(new HashSet<String>(Arrays.asList(id1, id2)), tableAttributes);
		System.err.println("multiple tables and ids:");
		for (Map.Entry<String, Map<String, Object>> entry : result2.entrySet()) {
			for (Map.Entry<String, Object> e : entry.getValue().entrySet()) {
				System.err.println(entry.getKey() + " - " + e.getKey() + " - " + e.getValue());
			}
		}
		Map<String, Map<String, Object>> result2All = jsonPersistence.getForMultipleIdAndMultipleTablesAll(new HashSet<String>(Arrays.asList(id1, id2)), new ArrayList<String>(tableAttributes.keySet()));
		System.err.println("multiple tables and ids all:");
		for (Map.Entry<String, Map<String, Object>> entry : result2All.entrySet()) {
			for (Map.Entry<String, Object> e : entry.getValue().entrySet()) {
				System.err.println(entry.getKey() + " - " + e.getKey() + " - " + e.getValue());
			}
		}

	}
	
	@Test
	public void testUpdateAttributes() {
		Gson gson = new Gson();
		String id = UUID.randomUUID().toString().substring(0, 4);
		String attribute = "spaceship"+id;
		JsonPersistence jsonPersistence = injector.getInstance(JsonPersistence.class);
		Map<String, Object> spaceship = new HashMap<String, Object>();
		spaceship.put("name", "mrs");
		spaceship.put("crewmembers", 8);
		jsonPersistence.updateAttribute(TABLE_NAME, id, attribute, gson.toJson(spaceship));
	}
	
	@Test
	public void testDatabaseObject() {
		String id = "dbobj1";
		DatabaseObject databaseObject = new DatabaseObject();
		databaseObject.Set("enabled", true);
		databaseObject.Set("number", 99);
		DatabaseObject subDatabaseObject = new DatabaseObject();
		subDatabaseObject.Set("str", "hello world");
		databaseObject.Set("sub1", subDatabaseObject);
		Database database = new Database(injector.getInstance(JsonPersistence.class));
		database.saveDatabaseObject(TABLE_NAME, id, databaseObject);
		
		DatabaseObject loadedDbObj = database.loadDatabaseObject(TABLE_NAME, id);
		assertEquals(99,loadedDbObj.GetInt("number"));
		assertEquals(true,loadedDbObj.Getboolean("enabled"));
		DatabaseObject loadedDbObj2 = loadedDbObj.GetObject("sub1");
		assertEquals("hello world",loadedDbObj2.GetString("str"));
		
	}
	

	private CreateTableRequest createTable(String tableName, Pair<String, String> hash, Pair<String, String> range) {
		ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
		if (hash != null) {
			attributeDefinitions.add(new AttributeDefinition()
            .withAttributeName(hash.left)
            .withAttributeType(hash.right));
		}
        if (range != null) {
            attributeDefinitions.add(new AttributeDefinition()
        	.withAttributeName(range.left)
        	.withAttributeType(range.right));
        	
        }

        ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
        if (hash != null) {
        keySchema.add(new KeySchemaElement()
            .withAttributeName(hash.left)
            .withKeyType(KeyType.HASH));
        }
        if (range != null) {
            keySchema.add(new KeySchemaElement()
        	.withAttributeName(range.left)
        	.withKeyType(KeyType.RANGE));        	
        }

        CreateTableRequest request = new CreateTableRequest()
            .withTableName(tableName)
            .withKeySchema(keySchema)
            .withAttributeDefinitions(attributeDefinitions)
            .withProvisionedThroughput(new ProvisionedThroughput()
                .withReadCapacityUnits(5L)
                .withWriteCapacityUnits(6L));		
		
		return request;
	}
	
	protected static void waitForTableToBeDeleted(AmazonDynamoDB dynamoDB, String tableName) throws InterruptedException {
	    System.out.println("Waiting for " + tableName + " to become ACTIVE...");
	 
	    long startTime = System.currentTimeMillis();
	    long endTime = startTime + (10 * 60 * 1000);
	    while ( System.currentTimeMillis() < endTime ) {
	        try {
	            DescribeTableRequest request = new DescribeTableRequest()
	                 .withTableName(tableName);
	            ListTablesResult listTableResult = dynamoDB.listTables();
	            if ( listTableResult == null ) continue;
	 
	            if (!listTableResult.getTableNames().contains(tableName)) {
	            	System.out.println(tableName + " not in list tables");
	            	return;
	            }
	        } catch ( AmazonServiceException ase ) {
	            if (!ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException"))
	                throw ase;
	        }
	        Thread.sleep(1000 * 5);
	    }
	 
	    throw new RuntimeException("Table " + tableName + " never went active");
	}
	
	protected static void waitForTableToBecomeAvailable(AmazonDynamoDB dynamoDB, String tableName) throws InterruptedException {
	    System.out.println("Waiting for " + tableName + " to become ACTIVE...");
	 
	    long startTime = System.currentTimeMillis();
	    long endTime = startTime + (10 * 60 * 1000);
	    while ( System.currentTimeMillis() < endTime ) {
	        try {
	            DescribeTableRequest request = new DescribeTableRequest()
	                 .withTableName(tableName);
	            TableDescription table = dynamoDB.describeTable(request).getTable();
	            if ( table == null ) continue;
	 
	            String tableStatus = table.getTableStatus();
	            System.out.println("  - current state: " + tableStatus);
	            if ( tableStatus.equals(TableStatus.ACTIVE.toString()) )
	                return;
	        } catch ( AmazonServiceException ase ) {
	            if (!ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException"))
	                throw ase;
	        }
	        Thread.sleep(1000 * 5);
	    }
	 
	    throw new RuntimeException("Table " + tableName + " never went active");
	}

}
