package net.omnidrone.logic.persistence;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import net.omnidrone.logic.session.DatabaseObject;
/**
 * DatabaseObject class represents a BigDB Database Object that has a Key and a collection
     of named properties.
     There are methods for setting, getting, and removing properties, and changes
     to the object can be persisted to BigDB with the Save() method.
     Database objects are very similar to JSON objects and are most easily visualized
     as such. 
     For example, we can visualize a car object like this:
     	'car20312' = { Name: 'OldSpeedy', Stats:{ Acceleration: 3.2, TopSpeed: 50,
     				   Steering: 5, WorkingBreaks:false } LatestLapTimes: ['20:34','22:03','22:30']
         }
 */   
public class DatabaseObjectTest {

	/**
	 * If we then want to create this object in BigDB, we would have to do this:
     	DatabaseObject car = new DatabaseObject(); car.Set("Name", "OldSpeedy");
     	//Create nested stats 
     	object DatabaseObject stats = new DatabaseObject();
     	stats.Set("Acceleration", 3.2); 
     	stats.Set("TopSpeed", 50); 
     	stats.Set("Steering", 5); 
     	stats.Set("WorkingBreaks", false); 
     	car.Set("Stats", stats); 
     	//Create laptimes 
     	array DatabaseArray laptimes = new DatabaseArray(); 
     	laptimes.Add("20:34");
        laptimes.Add("22:03"); 
        laptimes.Add("22:30"); 
        car.Set("LatestLapTimes", laptimes);
        //Save car to table "Cars" under key "car20312" 
        PlayerIO.BigDB.CreateObject("Cars", "car20312", car, null); 
	*/
	@Test
	public void testObjectCreation(){
		
	}
	
	/**	
	  To make it easier to work with nested objects, you can reference them directly
	      by separating the property names with a dot. Using this, we can set the stats
	      properties of the car like this instead:
	      DatabaseObject car = new DatabaseObject(); car.Set("Stats.Acceleration",
	      3.2); car.Set("Stats.TopSpeed", 50); car.Set("Stats.Steering", 5); car.Set("Stats.WorkingBreaks",
	      false);
	*/
	@Test
	public void testNestedObjectCreation(){
		
	}
	/**
   	When you want to read properties from a database object, you have to call
      the corresponding get method indicating the type of the property.
      PlayerIO.BigDB.Load("Cars", "car20312", delegate(DatabaseObject car) { 
      	string name = car.GetString("Name"); 
      	float acceleration = car.GetFloat("Stats.Acceleration");
        int topspeed = car.GetInt("Stats.TopSpeed"); 
      });
    */
	@Test
	public void testObjectReading(){
		
	}
	
	/**
	 *  
	     BigDB doesn't enforce anything on your objects. Two objects in the same table
         do not need to have the same properties set, and do not need to have the
         same type on the same property. If you try to get a property that is missing,
         or if you are using a get method of the wrong type, you will get an exception.
         PlayerIO.BigDB.Load("Cars", "car20312", delegate(DatabaseObject car) { //Name
         is a string, throws exception! int name = car.GetInt("Name"); //Color does
         not exist, throws exception! string color = car.GetString("Color"); });
        
	 * 
	 */
	@Test
	public void testObjectReadingHandlingExceptions(){
		
	}
	
	/**
	 * One way of avoiding this is to first check the object if a certain property
         exists before reading this. You can do this with the Contains method.
         PlayerIO.BigDB.Load("Cars", "car20312", delegate(DatabaseObject car) { if
         (!car.Contains("Name")) { car.Set("Name", "Unknown"); } if (car.Contains("Stats.Steering"))
         { //...  } });
	 */
	@Test
	public void testObjectReadingCheckIfContains(){
		
	}
	
	/**
	 * Another way of avoiding errors is to specify a default value when calling
         a get method. If the object doesn't contain the property, the method will
         return your default value instead.
         PlayerIO.BigDB.Load("Cars", "car20312", delegate(DatabaseObject car) { string
         color = car.GetString("Color", "Red"); });
	 */
	@Test
	public void testObjectReadingWithDefaultValues(){
		
	}
	
	
	/**
	 * 
	     You can also easily remove properties from your object with the Remove method.
         PlayerIO.BigDB.Load("Cars", "car20312", delegate(DatabaseObject car) { car.Remove("Name");
         car.Remove("Stats.TopSpeed"); if (!car.Contains("Name")) { //Name property
         should be removed now.  } });
	 */
	@Test
	public void testObjectRemovingProperties(){
		
	}
	
	/**
	 * 
	     Finally, to persist any changes you've made to an object you can call the
         Save method. Note that you can only call Save on an object that exist in
         the database. This means that the first time you save an object you have
         to call the PlayerIO.BigDB.CreateObject method, but all subsequent times
         you can call the Save method. There is also a property called ExistsInDatabase
         that you can use to check if it has been created or not.
         car.Set("Name", "NewSpeedy"); car.Set("Stats.Acceleration", 5.0); if (car.ExistsInDatabase)
         { car.Save(); } else { PlayerIO.BigDB.CreateObject("Cars", "car12345", null);
         }
    
	 */
	@Test
	public void testObjectSaving(){
		
	}
	
	/**
	     The Save method is asynchronous just like the CreateObject method in PlayerIO.BigDB,
         and you can specify a callback if you wish to do something when the object
         actually has been saved.
         car.Set("Name", "NewSpeedy"); car.Set("Stats.Acceleration", 5.0); car.Save(delegate()
         { //On successful save...  });
	 */
	@Test
	public void testObjectSavingWithCallback(){
		
	}
	
	@Test
	public void testBasic() throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		TypeFactory typeFactory = mapper.getTypeFactory();
		MapType mapType = typeFactory.constructMapType(HashMap.class, String.class, Object.class);
		
		String json = "{\"key\":12, \"key2\":{\"key3\":\"hello world\"}}";
		Map<String, Object> value = mapper.readValue(json, mapType);
		for (Map.Entry<String, Object> entry : value.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		DatabaseObject dbObj = new DatabaseObject(json);
		assertEquals(12,dbObj.GetInt("key"));
		DatabaseObject dbObj2 = dbObj.GetObject("key2");
		assertEquals("hello world",dbObj2.GetString("key3"));
		
	}

	@Test
	public void testExpression() throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		TypeFactory typeFactory = mapper.getTypeFactory();
		MapType mapType = typeFactory.constructMapType(HashMap.class, String.class, Object.class);
		
		String json = "{\"key\":12, \"key2\":{\"key3\":\"hello world\", \"key4\":{\"key5\":3.0}}}";
		Map<String, Object> value = mapper.readValue(json, mapType);
		for (Map.Entry<String, Object> entry : value.entrySet()) {
			System.err.println(entry.getKey() + " - " + entry.getValue());
		}
		DatabaseObject dbObj = new DatabaseObject(json);
		assertEquals(12,dbObj.GetInt("key"));
		DatabaseObject dbObj2 = dbObj.GetObject("key2");
		assertEquals("hello world",dbObj2.GetString("key3"));
		DatabaseObject dbObj3 = dbObj2.GetObject("key4");
		assertEquals(3.0d,dbObj3.GetDouble("key5"), 0.0);
		
		dbObj.Set("key2.key4.key5", 4.0);
		dbObj3 = dbObj2.GetObject("key4");
		assertEquals(4.0d,dbObj3.GetDouble("key5"), 0.0);
		DatabaseObject dbObj4 = dbObj.GetObject("key2.key4");
		assertEquals(4.0d,dbObj4.GetDouble("key5"), 0.0);
		
	}
	
	@Test
	public void testJsonFromDatabaseObject() throws Exception{
		String table = "table";
		String id = "dbobj1";
		DatabaseObject databaseObject = new DatabaseObject();
		databaseObject.Set("enabled", true);
		databaseObject.Set("number", 99);
		DatabaseObject subDatabaseObject = new DatabaseObject();
		subDatabaseObject.Set("str", "hello world");
		databaseObject.Set("sub1", subDatabaseObject);
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = databaseObject.getPrepared(databaseObject.getValue());
		System.err.println(map.toString());
		String json = mapper.writeValueAsString(map);
		//String json = mapper.writeValueAsString(databaseObject.getValue());
		System.err.println(json);
	}

}
