package net.omnidrone.logic;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.junit.Test;

import net.omnidrone.logic.domain.Spaceship;
import net.omnidrone.logic.session.JsonHelper;

public class JsonTest {

	@Test
	public void testJsonFromDynamo() {
		//Gson gson = new Gson();
		JsonHelper jsonHelper = new JsonHelper();
		String dynamoJson = "{\"id\":\"dedb\",\"name\":\"5th\",\"velocity\":56,\"cargo\":\"apples\",\"crewMembers\":6}";
		//Spaceship s = gson.fromJson(dynamoJson, Spaceship.class);
		System.err.println(dynamoJson);
		dynamoJson = dynamoJson.replace("\\", "");
		System.err.println(dynamoJson);
		Spaceship s = jsonHelper.fromJson(dynamoJson, Spaceship.class);
		assertEquals("dedb", s.id);
		assertEquals("5th", s.name);
		assertEquals(56, s.velocity);
		assertEquals("apples", s.cargo);
		assertEquals(6, s.crewMembers);
	}
	
	
	@Test
	public void testNestedJsonConversion() throws Exception{
		Map<String, Object> topLevel = new HashMap<String, Object>();
		Map<String, Object> middleLevel = new HashMap<String, Object>();
		Map<String, Object> bottomLevel = new HashMap<String, Object>();
		topLevel.put("level", 1);
		topLevel.put("middle-level", middleLevel);
		middleLevel.put("level", 2);
		middleLevel.put("bottom-level", bottomLevel);
		bottomLevel.put("level", "3rd");
		
		ObjectMapper mapper = new ObjectMapper();
	    
		String json = mapper.writeValueAsString(topLevel);
		
		System.err.println(json);
		Map<String, Object> convertedTop = (Map<String, Object>)mapper.readValue(json, Map.class);
		Map<String, Object> convertedMiddle = (Map<String, Object>)convertedTop.get("middle-level");
		Map<String, Object> convertedBottom = (Map<String, Object>)convertedMiddle.get("bottom-level");

		assertEquals(convertedTop.get("level"), 1);
		assertEquals(convertedMiddle.get("level"), 2);
		assertEquals(convertedBottom.get("level"), "3rd");
		String json2 = mapper.writeValueAsString(convertedTop);
		System.err.println(json2);
		assertEquals(json, json2);
		
		
	}
	
	@Test
	public void testAvroJson() throws Exception{
		String json = "{\"order_id\":1,\"customer_id\":666,\"total\":9.98,\"order_details\":[{\"quantity\":2,\"total\":9.98,\"product_detail\":{\"product_id\":12,\"product_name\":\"jamon\",\"product_status\":\"AVAILABLE\",\"product_category\":[\"meat\"],\"price\":4.99,\"product_hash\":\"\\U0000\\U0000\\U0000\\U0000\\U0000\"}}]}";
		//json = json.replace("\\U0000", "\\u0000");
		json = json.replace("\\U0000", "");
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.configure(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, false);
		mapper.configure(Feature.WRITE_NULL_PROPERTIES, false);
		mapper.configure(Feature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper = mapper.setSerializationInclusion(Inclusion.NON_NULL);
		mapper = mapper.setSerializationInclusion(Inclusion.NON_EMPTY);
	    Map map = mapper.readValue(json, Map.class);
		System.err.println(mapper.writeValueAsString(map));
	}
}
