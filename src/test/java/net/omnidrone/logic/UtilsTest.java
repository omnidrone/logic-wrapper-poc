package net.omnidrone.logic;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testRandom() throws Exception {
		SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
		// generate a random number
		String randomNum = new Integer(prng.nextInt()).toString();

		// get its digest
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		byte[] result = sha.digest(randomNum.getBytes());
		System.err.println(result.length);
		System.out.println("Random number: " + randomNum);
		String hexString = hexEncode(result);
		System.out.println("Message digest: " + hexString);
		System.out.println("Message digest: " + hexString.substring(0, 8));

		String[] allowedChars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
				"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
				"9" };
		
		for (int j = 0; j < 10; j++) {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < 16; i++) {

			int index = prng.nextInt(allowedChars.length);
			builder.append(allowedChars[index]);
		}
		System.err.println(builder.toString());
		}
	}

	static private String hexEncode(byte[] aInput) {
		StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}
}
