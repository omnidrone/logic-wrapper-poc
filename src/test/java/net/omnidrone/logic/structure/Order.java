package net.omnidrone.logic.structure;

import java.util.List;

public class Order implements JsonSerializable<Order>{
	public int orderId;
	public int customerId;
	public double total;
	public List<OrderDetails> orderDetails;
	
	@Override
	public String toJson() {
		return null;
	}
	@Override
	public Order fromJson(String json) {
		return null;
	}
}

