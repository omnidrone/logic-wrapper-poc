package net.omnidrone.logic.structure;

import java.util.Set;

public class Product implements JsonSerializable<Product>{
	public long productId;
	public String productName;
	public ProductStatus productStatus;
	public Set<String> productCategories;
	public double price;
	
	@Override
	public String toJson() {
		return null;
	}
	@Override
	public Product fromJson(String json) {
		return null;
	}
}

enum ProductStatus {
	AVAILABLE, OUT_OF_STOCK, ONLY_FEW_LEFT
}
