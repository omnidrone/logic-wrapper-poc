package net.omnidrone.logic.structure;

public interface JsonSerializable<T> {

	String toJson();
	
	T fromJson(String json);
}
