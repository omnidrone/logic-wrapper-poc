package net.omnidrone.logic;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Pattern;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.omnidrone.game.protobuf.GameProtos;
import net.omnidrone.game.protobuf.GameProtos.GameCommandResponse;

public class ClientHandler extends SimpleChannelInboundHandler<GameProtos.GameCommandResponse> {

    // Stateful properties
    private volatile Channel channel;
    private final BlockingQueue<GameProtos.GameCommandResponse> answer = new LinkedBlockingQueue<GameProtos.GameCommandResponse>();

    public ClientHandler() {
        super(false);
    }

    public void sendSpaceshipCommand(GameProtos.GameCommand cmd) {
        channel.writeAndFlush(cmd);
        System.err.println("spaceship command flushed");
    }

    public GameProtos.GetSpaceshipResponse getSpaceshipResponse() {
        GameProtos.GameCommandResponse response;
        boolean interrupted = false;
        for (;;) {
            try {
                response = answer.take();
                break;
            } catch (InterruptedException ignore) {
                interrupted = true;
            }
        }

        if (interrupted) {
            Thread.currentThread().interrupt();
        }
        Any any = response.getCommand();
        try {
			return any.unpack(GameProtos.GetSpaceshipResponse.class);
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			return null;
		}
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) {
        channel = ctx.channel();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GameCommandResponse msg) throws Exception {
    	System.err.println("reading response");
        //channel = ctx.channel();
    	answer.add(msg);
    }
    
}
