package net.omnidrone.logic;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.omnidrone.logic.domain.Spaceship;
import net.omnidrone.savage.commons.Pair;
import net.omnidrone.savage.config.EnvironmentProvider;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBConfiguration;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBLifecycle;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import com.amazonaws.services.dynamodbv2.model.DeleteTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TableStatus;
import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class CreateDynamoTables {

	static Injector injector;
	private static boolean setupDone = false;

	@BeforeClass
	public static void init() {
		injector = Guice.createInjector(new AbstractModule() {

			@Override
			protected void configure() {

				DynamoDBConfiguration config = new DynamoDBConfiguration();
				config.accessKey = "AKIAIMGAFWX3NIUUWMBA";
				config.secretKey = "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";
				config.dynamoTestDb = false;
				config.regionName = "eu-west-1";
				bind(DynamoDBConfiguration.class).toInstance(config);
				bind(DynamoDBLifecycle.class);
			}
		});
		
	}

	@Before
	public void cleanUp() {
		if (!setupDone) {
			final EnvironmentProvider envProvider = injector.getInstance(EnvironmentProvider.class);
			final DynamoDBLifecycle dynamoDBLifecycle = injector.getInstance(DynamoDBLifecycle.class);
			dynamoDBLifecycle.onStartUp();

			AmazonDynamoDB dynamoDB = dynamoDBLifecycle.getAmazonDynamoDB();// =
																			// new
																			// AmazonDynamoDBClient(new
																			// Credentials(config));
//			String tableName = "spaceship";
//
//			try {
//				dynamoDB.deleteTable(deleteTable(tableName));
//				waitForTableToBeDeleted(dynamoDB, tableName);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			try {
//				dynamoDB.createTable(createTable(tableName, new Pair<String, String>("Id", "S"), null));
//				waitForTableToBecomeAvailable(dynamoDB, tableName);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
		}
		setupDone = true;

	}

	protected static void waitForTableToBecomeAvailable(AmazonDynamoDB dynamoDB, String tableName)
			throws InterruptedException {
		System.out.println("Waiting for " + tableName + " to become ACTIVE...");

		long startTime = System.currentTimeMillis();
		long endTime = startTime + (10 * 60 * 1000);
		while (System.currentTimeMillis() < endTime) {
			try {
				DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
				TableDescription table = dynamoDB.describeTable(request).getTable();
				if (table == null)
					continue;

				String tableStatus = table.getTableStatus();
				System.out.println("  - current state: " + tableStatus);
				if (tableStatus.equals(TableStatus.ACTIVE.toString()))
					return;
			} catch (AmazonServiceException ase) {
				if (!ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException"))
					throw ase;
			}
			Thread.sleep(1000 * 5);
		}

		throw new RuntimeException("Table " + tableName + " never went active");
	}

	protected static void waitForTableToBeDeleted(AmazonDynamoDB dynamoDB, String tableName)
			throws InterruptedException {
		System.out.println("Waiting for " + tableName + " to become ACTIVE...");

		long startTime = System.currentTimeMillis();
		long endTime = startTime + (10 * 60 * 1000);
		while (System.currentTimeMillis() < endTime) {
			try {
				DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
				ListTablesResult listTableResult = dynamoDB.listTables();
				if (listTableResult == null)
					continue;

				if (!listTableResult.getTableNames().contains(tableName)) {
					System.out.println(tableName + " not in list tables");
					return;
				}
			} catch (AmazonServiceException ase) {
				if (!ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException"))
					throw ase;
			}
			Thread.sleep(1000 * 5);
		}

		throw new RuntimeException("Table " + tableName + " never went active");
	}

	@Test
	public void test() {
		final DynamoDBLifecycle dynamoDBLifecycle = injector.getInstance(DynamoDBLifecycle.class);

		AmazonDynamoDB awsDynamoDB = dynamoDBLifecycle.getAmazonDynamoDB();
		DynamoDB dynamoDB = new DynamoDB(awsDynamoDB);
		
		final String TABLE_NAME = "spaceship";
		final String UID = "10";
		
		Spaceship spaceship = new Spaceship();
		spaceship.name = "GX";
		spaceship.velocity = 999;
		spaceship.cargo = "plasma";
		
		Gson gson = new Gson();
		Table table = dynamoDB.getTable(TABLE_NAME);
		Item item = new Item().withPrimaryKey("Id", UID).withJSON("document", gson.toJson(spaceship));

		table.putItem(item);
		
		
		Item documentItem = table.getItem(new GetItemSpec().withPrimaryKey("Id", UID).withAttributesToGet("document"));
		String json = documentItem.getJSONPretty("document");
		
		Spaceship spaceship2 = gson.fromJson(json, Spaceship.class);
		assertEquals(spaceship.name, spaceship2.name);
		assertEquals(spaceship.velocity, spaceship2.velocity);
		assertEquals(spaceship.cargo, spaceship2.cargo);
		
//		item = new Item().withPrimaryKey("Id", UID).withMap("document", new ValueMap().withString("cargo", "gold"));
//		table.putItem(item);
//		
//		item = table.getItem(new GetItemSpec().withPrimaryKey("Id", UID).withProjectionExpression("document.cargo"));
//		System.err.println(item);
//
		table.updateItem(
				  new UpdateItemSpec()
				      .withPrimaryKey("Id", UID)
				      .withUpdateExpression("SET document.cargo = :cargo")
				      .withValueMap(new ValueMap().withString(":cargo", "apples")));

		item = table.getItem(new GetItemSpec().withPrimaryKey("Id", UID).withProjectionExpression("document.cargo"));
		System.err.println(item);

	}
//	@Test
//	public void testWarPersistence() {
//
//		final ValhallaPersistenceDynamoDB persistence = injector.getInstance(ValhallaPersistenceDynamoDB.class);
//		final War war = new War();
//		war.warId = UUID.randomUUID().toString();
//		war.brotherhoodId1 = 1l;
//		war.brotherhoodId2 = 2l;
//		war.warStartTime = System.currentTimeMillis();
//		war.warEndTime = System.currentTimeMillis() + 1000;
//		war.warState = WarState.DECLARED;
//
//		persistence.updateWar(war.warId, war);
//
//		final War war2 = persistence.getWar(war.warId);
//		assertEquals(war.warId, war2.warId);
//		assertEquals(war.brotherhoodId1, war2.brotherhoodId1);
//		assertEquals(war.brotherhoodId2, war2.brotherhoodId2);
//		assertEquals(war.warStartTime, war2.warStartTime);
//		assertEquals(war.warEndTime, war2.warEndTime);
//		assertEquals(war.warState, war2.warState);
//		Set<String> warIds = new HashSet<String>();
//		warIds.add(war.warId);
//		final Map<String, War> wars = persistence.getWars(warIds);
//		final War war3 = wars.get(war.warId);
//		assertEquals(war.warId, war3.warId);
//		assertEquals(war.brotherhoodId1, war3.brotherhoodId1);
//		assertEquals(war.brotherhoodId2, war3.brotherhoodId2);
//		assertEquals(war.warStartTime, war3.warStartTime);
//		assertEquals(war.warEndTime, war3.warEndTime);
//		assertEquals(war.warState, war3.warState);
//
//		final War warUpdate = new War();
//		warUpdate.warId = war.warId;
//		warUpdate.warState = WarState.STARTED;
//		persistence.updateWar(war.warId, warUpdate);
//		final War war4 = persistence.getWar(war.warId);
//		assertEquals(war.warId, war4.warId);
//		assertEquals(war.brotherhoodId1, war4.brotherhoodId1);
//		assertEquals(war.brotherhoodId2, war4.brotherhoodId2);
//		assertEquals(war.warStartTime, war4.warStartTime);
//		assertEquals(war.warEndTime, war4.warEndTime);
//		assertEquals(WarState.STARTED, war4.warState);
//
//		persistence.updateWarState(war.warId, WarState.ENDED);
//		final War war5 = persistence.getWar(war.warId);
//		assertEquals(war.warId, war5.warId);
//		assertEquals(war.brotherhoodId1, war5.brotherhoodId1);
//		assertEquals(war.brotherhoodId2, war5.brotherhoodId2);
//		assertEquals(war.warStartTime, war5.warStartTime);
//		assertEquals(war.warEndTime, war5.warEndTime);
//		assertEquals(WarState.ENDED, war5.warState);
//
//	}
	private CreateTableRequest createTable(String tableName, Pair<String, String> hash, Pair<String, String> range) {
		ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
		if (hash != null) {
			attributeDefinitions.add(new AttributeDefinition()
            .withAttributeName(hash.left)
            .withAttributeType(hash.right));
		}
        if (range != null) {
            attributeDefinitions.add(new AttributeDefinition()
        	.withAttributeName(range.left)
        	.withAttributeType(range.right));
        	
        }

        ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
        if (hash != null) {
        keySchema.add(new KeySchemaElement()
            .withAttributeName(hash.left)
            .withKeyType(KeyType.HASH));
        }
        if (range != null) {
            keySchema.add(new KeySchemaElement()
        	.withAttributeName(range.left)
        	.withKeyType(KeyType.RANGE));        	
        }

        CreateTableRequest request = new CreateTableRequest()
            .withTableName(tableName)
            .withKeySchema(keySchema)
            .withAttributeDefinitions(attributeDefinitions)
            .withProvisionedThroughput(new ProvisionedThroughput()
                .withReadCapacityUnits(5L)
                .withWriteCapacityUnits(6L));		
		
		return request;
	}

	private DeleteTableRequest deleteTable(String tableName) {
		return new DeleteTableRequest().withTableName(tableName);
	}

}
