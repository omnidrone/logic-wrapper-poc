package net.omnidrone.logic;

import com.google.protobuf.ExtensionRegistry;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import net.omnidrone.logic.netty.LogicChannelHandler.IdleHandler;
import net.omnidrone.game.protobuf.GameProtos;

public class ClientInitializer extends ChannelInitializer<SocketChannel> {
	
    @Override
    public void initChannel(SocketChannel ch) {
    	ExtensionRegistry registry = ExtensionRegistry.newInstance();
		GameProtos.registerAllExtensions(registry);
        ChannelPipeline p = ch.pipeline();

        p.addLast(new ProtobufVarint32FrameDecoder());
        p.addLast(new ProtobufDecoder(GameProtos.GameCommandResponse.getDefaultInstance(), registry));

        p.addLast(new ProtobufVarint32LengthFieldPrepender());
        p.addLast(new ProtobufEncoder());

        p.addLast(new ClientHandler());
        
        p.addLast(new IdleStateHandler(10, 10, 0));
		p.addLast(new IdleHandler());
    }
    
    public class IdleHandler extends ChannelDuplexHandler {
	     @Override
	     public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
	         if (evt instanceof IdleStateEvent) {
	             IdleStateEvent e = (IdleStateEvent) evt;
	             if (e.state() == IdleState.READER_IDLE) {
	                 System.err.println("Reader has been idle!");
	            	 ctx.close();
	             } else if (e.state() == IdleState.WRITER_IDLE) {
	                 System.err.println("Writer has been idle!");
	                 //ctx.writeAndFlush(new PingMessage());
	             }
	         }
	     }
	 }

}
