package net.omnidrone.logic.services;


import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.service.DatabaseRPCGrpc;
import net.omnidrone.service.ServiceDb.GetRequest;
import net.omnidrone.service.ServiceDb.GetResponse;
import net.omnidrone.service.ServiceDb.UpdateRequest;
import net.omnidrone.service.ServiceDb.UpdateResponse;

@Singleton
public class DbApiService {
	private static final Logger logger = LoggerFactory.getLogger(DbApiService.class);

	@Inject
	JsonPersistence jsonPersistence;
	
	ObjectMapper mapper = new ObjectMapper();
	
	  /* The port on which the server should run */
	  private int port = 50052;
	  private Server server;

	  public void start() throws Exception {
	    server = ServerBuilder.forPort(port)
	        .addService(DatabaseRPCGrpc.bindService(new DbApiService.DatabaseRPCImpl()))
	        .build()
	        .start();
	    logger.info("Server started, listening on " + port);
	    Runtime.getRuntime().addShutdownHook(new Thread() {
	      @Override
	      public void run() {
	        // Use stderr here since the logger may have been reset by its JVM shutdown hook.
	        System.err.println("*** shutting down gRPC server since JVM is shutting down");
	        DbApiService.this.stop();
	        System.err.println("*** server shut down");
	      }
	    });
	  }

	  private void stop() {
	    if (server != null) {
	      server.shutdown();
	    }
	  }

	  /**
	   * Await termination on the main thread since the grpc library uses daemon threads.
	   */
	  private void blockUntilShutdown() throws InterruptedException {
	    if (server != null) {
	      server.awaitTermination();
	    }
	  }

	  /**
	   * Main launches the server from the command line.
	   */
	  public static void main(String[] args) throws Exception {
	    final DbApiService server = new DbApiService();
	    server.start();
	    server.blockUntilShutdown();
	  }

	  private class DatabaseRPCImpl implements DatabaseRPCGrpc.DatabaseRPC {


		@Override
		public void get(GetRequest getRequest, StreamObserver<GetResponse> responseObserver) {
			String tableId = getRequest.getTableId();
			String id = getRequest.getId();
			Map<String, Object> map = jsonPersistence.get(tableId, id);
			
			ObjectMapper mapper = new ObjectMapper();
			String json;
			try {
				json = mapper.writeValueAsString(map);
				GetResponse getResponse = GetResponse.newBuilder().setJson(json).build();
				responseObserver.onNext(getResponse);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				responseObserver.onError(e);
			}
			responseObserver.onCompleted();
		}

		@Override
		public void update(UpdateRequest updateRequest, StreamObserver<UpdateResponse> responseObserver) {
			String tableId = updateRequest.getTableId();
			String id = updateRequest.getId();
			String json = updateRequest.getJson();
			jsonPersistence.update(tableId, id, json);
			
			responseObserver.onCompleted();
		}
	  }
}
