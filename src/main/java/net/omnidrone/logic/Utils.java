package net.omnidrone.logic;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Utils {
	static SecureRandom prng;
	static MessageDigest sha;

	static {
		try {
			prng = SecureRandom.getInstance("SHA1PRNG");
			sha = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static String generateId() {
		// generate a random number
		String randomNum = new Integer(prng.nextInt()).toString();
		// get its digest
		byte[] result = sha.digest(randomNum.getBytes());
		String hexString = hexEncode(result);
		return hexString.substring(0, 8);
//
//		String[] allowedChars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
//				"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
//				"9" };
//		
//			StringBuilder builder = new StringBuilder();
//			for (int i = 0; i < 16; i++) {
//
//			int index = prng.nextInt(allowedChars.length);
//			builder.append(allowedChars[index]);
//		}
//		System.err.println(builder.toString());
	}

	static private String hexEncode(byte[] aInput) {
		StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}
}
