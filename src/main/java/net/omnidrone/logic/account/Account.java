package net.omnidrone.logic.account;

import java.util.UUID;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import net.omnidrone.logic.Utils;
import net.omnidrone.savage.persistence.redis.RedisNode;
import net.omnidrone.savage.persistence.redis.RedisNodeFactory;
import redis.clients.jedis.Transaction;

@Singleton
public class Account {

	public static final String EXTERNAL_TO_UID = "eid-uid-mapping";
	public static final String UID_TO_EXTERNAL = "uid-eid-mapping";
	
	@Inject
	RedisNodeFactory redisFactory;
	
	public String getUserId(String externalId) {
		RedisNode redis = redisFactory.getMasterNode();
		String userId = redis.call(jedis -> jedis.hget(EXTERNAL_TO_UID, externalId));
		if (userId != null) {
			return userId;
		}
		userId = Utils.generateId();
		while (checkExists(redis, userId)) {
			userId = Utils.generateId();
		}
		final String uid = userId;
		redis.call(jedis -> {
			Transaction tx = jedis.multi();
			tx.hset(EXTERNAL_TO_UID, externalId, uid);
			tx.hset(UID_TO_EXTERNAL, uid, externalId);
			return tx.exec();
		});
		return uid;
	}
	
	private boolean checkExists(RedisNode redis, String uid) {
		return redis.call(jedis -> jedis.hexists(UID_TO_EXTERNAL, uid));
	}
	
}
