package net.omnidrone.logic.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.BatchGetItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.BatchGetItemResult;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import net.omnidrone.logic.session.Key;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBConfiguration;

@Singleton
public class Persistence {

	@Inject
	DynamoDBConfiguration config;
	
	DynamoDB dynamoDB = null;

	private DynamoDB getClient() {
		if (dynamoDB == null) {
//			AmazonDynamoDBClient awsDynamoDB = new AmazonDynamoDBClient(new BasicAWSCredentials("", ""));
//			awsDynamoDB.setEndpoint("http://localhost:8000");
//			dynamoDB = new DynamoDB(awsDynamoDB);
			AmazonDynamoDBClient awsDynamoDB = new AmazonDynamoDBClient(new Credentials(config));
			Region region;
			if (config.regionName != null && !"".equals(config.regionName)) {
				region = RegionUtils.getRegion(config.regionName);
			} else {
				region = Region.getRegion(Regions.US_WEST_2);
			}
			awsDynamoDB.setRegion(region);
			dynamoDB = new DynamoDB(awsDynamoDB);
		}
		return dynamoDB;
	}
	
	private class Credentials implements AWSCredentials {

		private DynamoDBConfiguration config;
		
		Credentials(DynamoDBConfiguration config) {
			this.config = config;
		}
		
		@Override
		public String getAWSAccessKeyId() {
			return config.accessKey;
		}

		@Override
		public String getAWSSecretKey() {
			return config.secretKey;
		}
		
	}

//	public String getData(String userId, String tableName, String ... column) {
//		Table table = getClient().getTable(tableName);
//		if (table != null) {
//			Item documentItem = table
//					.getItem(new GetItemSpec().withPrimaryKey("Id", ""+userId).withAttributesToGet(column));
//
//			return documentItem != null ? documentItem.getJSON(column) : null;
//		}
//		return null;
//	}

	public Map<String, String> getDataFromTable(Key key) {
		return getDataFromTable(key.id, key.table, key.columns);
	}
	
	public Map<String, String> getDataFromTable(String userId, String tableName, String ... columns) {
		Table table = getClient().getTable(tableName);
		Map<String, String> result = new HashMap<String, String>();
		if (table != null) {
			Item documentItem = table
					.getItem(new GetItemSpec().withPrimaryKey("Id", ""+userId).withAttributesToGet(columns));

			if (documentItem != null) {
				for (String column : columns) {
					result.put(column, documentItem.getJSON(column));
				}
			}
		}
		return result;
	}

	public Map<String, String> getAllDataFromTable(String userId, String tableName) {
		Table table = getClient().getTable(tableName);
		Map<String, String> result = new HashMap<String, String>();
		if (table != null) {
			Item documentItem = table
					.getItem(new GetItemSpec().withPrimaryKey("Id", ""+userId));

			if (documentItem != null) {
				Map<String, Object> attributes = documentItem.asMap();
				for (Map.Entry<String, Object> entry : attributes.entrySet()) {
					result.put(entry.getKey(), documentItem.getJSON(entry.getKey()));
				}
			}
		}
		return result;
	}
	
	public Map<String, Map<String, String>> getDataFromTables(List<Key> keys) {
		TableKeysAndAttributes[] tkaas = new TableKeysAndAttributes[keys.size()];
		for (int i=0;i<0;i++) {
			Key key = keys.get(i);
			TableKeysAndAttributes tkaa = new TableKeysAndAttributes(key.table);
			tkaa.addPrimaryKey(new PrimaryKey("Id", key.id)).withProjectionExpression(StringUtils.join(key.columns,","));
			tkaas[i] = tkaa;
		}
		Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();
		
		BatchGetItemOutcome batchGetItem = getClient().batchGetItem(tkaas);
		if (batchGetItem != null) {
			BatchGetItemResult batchGetItemResult = batchGetItem.getBatchGetItemResult();
			Map<String, List<Map<String, AttributeValue>>> responseMap = batchGetItemResult.getResponses();
			for (Map.Entry<String, List<Map<String, AttributeValue>>> mapEntry : responseMap.entrySet()) {
				Map<String, String> itemMap = new HashMap<String, String>();
				String tableName = mapEntry.getKey();
				List<Map<String, AttributeValue>> items = mapEntry.getValue();
				for (Map<String, AttributeValue> item : items) {
					for (Map.Entry<String, AttributeValue> itemEntry : item.entrySet()) {
						itemMap.put(itemEntry.getKey(), itemEntry.getValue().getS());
					}
				}
				result.put(tableName, itemMap);
			
			}
		}
		return result;
	}


	public void updateData(String userId, String tableName, String column, String data) {
		Table table = getClient().getTable(tableName);
		Item item = new Item().withPrimaryKey("Id", ""+userId).withJSON(column, data);

		table.putItem(item);
	}

}
