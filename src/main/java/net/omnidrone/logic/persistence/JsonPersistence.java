package net.omnidrone.logic.persistence;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface JsonPersistence {

	Map<String, Object> getAttributes(String table, String id, List<String> attributes);
	
	Map<String, Object> getAttribute(String table, String id, String attribute);
	
	Map<String, Object> get(String table, String id);
	
	Map<String, Object> getMultipleIds(Set<String> ids, String table, List<String> attributes);		
		
	Map<String, Map<String, Object>> getForMultipleIdAndMultipleTables(Set<String> ids, Map<String, List<String>> tableAttributes);
		
	Map<String, Object> getFromMultipleTables(String id, Map<String, List<String>> tableAttributes);

	Map<String, Object> getMultipleIdsAll(Set<String> ids, String table);		
	
	Map<String, Map<String, Object>> getForMultipleIdAndMultipleTablesAll(Set<String> ids, List<String> tables);
		
	Map<String, Object> getFromMultipleTablesAll(String id, List<String> tables);
	
	void update(String table, String id, String json);

	void updateAttribute(String table, String id, String attribute, Object value);

	void updateAttributes(String table, String id, Map<String, Object> attributeValues);
	
	void delete(String table, String id);
	
	void deleteAttributes(String table, String id, String ... columns);
}
