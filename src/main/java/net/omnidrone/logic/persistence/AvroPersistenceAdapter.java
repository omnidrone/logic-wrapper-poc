package net.omnidrone.logic.persistence;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonEncoder;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.inject.Inject;

public class AvroPersistenceAdapter {

	@Inject
	JsonPersistence persistence;
	
	ObjectMapper mapper = new ObjectMapper();
	
	public byte[] get(String table, String id) throws Exception {
		Map<String, Object> map = persistence.get(table, id);
		String json = mapper.writeValueAsString(map);
		System.err.println("transform json to avro object");
		System.err.println(json);
		Schema schema = new Schema.Parser().parse(new File("/home/henrik/development/logic-wrapper-poc/avro/order.avsc"));
		byte[] bytes = jsonToAvro(json, schema);
		//test bytes
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
		DecoderFactory decoderFactory = new DecoderFactory();
		
		Decoder decoder = decoderFactory.binaryDecoder(bytes, 0, bytes.length, null);
		
		GenericRecord order = datumReader.read(null, decoder);
		System.err.println(order);
		
		System.err.println("Return bytes to C");
		return bytes;
	}
	
	
	void update(String table, String id, byte[] avroObject) throws Exception{
		int noBytes = avroObject.length;
		System.err.println("decode order, no bytes:" + noBytes);
		Schema schema = new Schema.Parser().parse(new File("/home/henrik/development/logic-wrapper-poc/avro/order.avsc"));
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
		DecoderFactory decoderFactory = new DecoderFactory();
		
		Decoder decoder = decoderFactory.binaryDecoder(avroObject, 0, noBytes, null);
		
		GenericRecord genericRecord = datumReader.read(null, decoder);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		JsonEncoder jsonEncoder = EncoderFactory.get().jsonEncoder(
				schema, out);
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
		datumWriter.write(genericRecord, jsonEncoder);
		jsonEncoder.flush();
		String json = new String(out.toByteArray());
		System.err.println("update json:");
		System.err.println(json);
		persistence.update(table, id, json);
	}
	
	public static byte[] jsonToAvro(String json, Schema schema) throws IOException {
	    InputStream input = null;
	    GenericDatumWriter<GenericRecord> writer = null;
	    Encoder encoder = null;
	    ByteArrayOutputStream output = null;
	    
	    try {
	        DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>(schema);
	        input = new ByteArrayInputStream(json.getBytes());
	        output = new ByteArrayOutputStream();
	        DataInputStream din = new DataInputStream(input);
	        writer = new GenericDatumWriter<GenericRecord>(schema);
	        Decoder decoder = DecoderFactory.get().jsonDecoder(schema, din);
	        encoder = EncoderFactory.get().binaryEncoder(output, null);
	        GenericRecord datum;
	        while (true) {
	            try {
	                datum = reader.read(null, decoder);
	            } catch (EOFException eofe) {
	                break;
	            }
	            writer.write(datum, encoder);
	        }
	        encoder.flush();
	        return output.toByteArray();
	    } finally {
	        try { input.close(); } catch (Exception e) { }
	    }
	}
	
	public static String avroToJson(byte[] avro, Schema schema) throws IOException {
	    boolean pretty = false;
	    GenericDatumReader<GenericRecord> reader = null;
	    JsonEncoder encoder = null;
	    ByteArrayOutputStream output = null;
	    try {
	        reader = new GenericDatumReader<GenericRecord>(schema);
	        InputStream input = new ByteArrayInputStream(avro);
	        output = new ByteArrayOutputStream();
	        DatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(schema);
	        encoder = EncoderFactory.get().jsonEncoder(schema, output, pretty);
	        Decoder decoder = DecoderFactory.get().binaryDecoder(input, null);
	        GenericRecord datum;
	        while (true) {
	            try {
	                datum = reader.read(null, decoder);
	            } catch (EOFException eofe) {
	                break;
	            }
	            writer.write(datum, encoder);
	        }
	        encoder.flush();
	        output.flush();
	        return new String(output.toByteArray());
	    } finally {
	        try { if (output != null) output.close(); } catch (Exception e) { }
	    }
	}
}
