package net.omnidrone.logic.persistence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.BatchGetItemOutcome;
import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.internal.ItemValueConformer;
import com.amazonaws.services.dynamodbv2.document.spec.BatchGetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.BatchGetItemResult;
import com.amazonaws.services.dynamodbv2.model.KeysAndAttributes;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.amazonaws.util.json.Jackson;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import net.omnidrone.savage.faultisolation.Group;
import net.omnidrone.savage.faultisolation.aop.FaultIsolate;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBConfiguration;

@Singleton
public class DynamoDBJsonPersistence implements JsonPersistence {
	private static final Logger logger = LoggerFactory.getLogger(DynamoDBJsonPersistence.class);

	public final static String DYNAMO_TABLE_ID = "Id";

	Gson gson = new Gson();
	DynamoDB dynamoDB;

	@Inject
	public DynamoDBJsonPersistence(DynamoDBConfiguration config) {
			// AmazonDynamoDBClient awsDynamoDB = new AmazonDynamoDBClient(new
			// BasicAWSCredentials("", ""));
			// awsDynamoDB.setEndpoint("http://localhost:8000");
			// dynamoDB = new DynamoDB(awsDynamoDB);
			AmazonDynamoDBClient awsDynamoDB = new AmazonDynamoDBClient(new Credentials(config));
			Region region;
			if (config.regionName != null && !"".equals(config.regionName)) {
				region = RegionUtils.getRegion(config.regionName);
			} else {
				region = Region.getRegion(Regions.US_WEST_2);
			}
			awsDynamoDB.setRegion(region);
			dynamoDB = new DynamoDB(awsDynamoDB);
		
	}

	private class Credentials implements AWSCredentials {

		private DynamoDBConfiguration config;

		Credentials(DynamoDBConfiguration config) {
			this.config = config;
		}

		@Override
		public String getAWSAccessKeyId() {
			return config.accessKey;
		}

		@Override
		public String getAWSSecretKey() {
			return config.secretKey;
		}

	}

	@Override
	@FaultIsolate(group = Group.DYNAMO, command = "select")
	public Map<String, Object> getAttribute(String tableName, String id, String attribute) {
		return getAttributes(tableName, id, Arrays.asList(attribute));
	}
	
	@Override
	@FaultIsolate(group = Group.DYNAMO, command = "select")
	public Map<String, Object> getAttributes(String tableName, String id, List<String> attributes) {

		Table table = dynamoDB.getTable(tableName);
			
			StringBuilder expression = new StringBuilder();
			boolean comma = false;
			Map<String, String> expressionAttributeNames = new HashMap<String, String>();
			for (String attribute : attributes) {
				if (comma) {
					expression.append(",");
				} else {
					comma = true;
				}
				if (attribute.contains(".")) {
					String[] parts = attribute.trim().split("\\.");
					List<String> path = new ArrayList<String>();
					for (int p=0;p<parts.length;p++) {
						path.add("#"+parts[p]);
						expressionAttributeNames.put("#" + parts[p], parts[p]);
					}
					expression.append(StringUtils.join(path, "."));
				}  else {
					expression.append("#").append(attribute);
					expressionAttributeNames.put("#" + attribute, attribute);
				}
				
				
			}
			System.err.println("EXPRESSION:" + expression.toString());
			GetItemSpec spec = new GetItemSpec()
				    .withPrimaryKey(DYNAMO_TABLE_ID, id)
				    .withProjectionExpression(expression.toString()).withNameMap(expressionAttributeNames)
				    .withConsistentRead(true);
				    
			Item documentItem = table
					.getItem(spec);

			if (documentItem != null) {
				Map<String, Object> docMap = documentItem.asMap();
				return docMap;
			} else {
				return Collections.emptyMap();
			}
//			Map<String, String> result = new HashMap<String, String>();
//			for (String key : docMap.keySet()) {
//				String json = documentItem.getJSON(key);
//				json = json.replaceAll("^\"|\"$", "");
//				result.put(key, json);
//			}
//			return result;
	}
	
	public Map<String, Object> get(String tableName, String id) {
		Table table = dynamoDB.getTable(tableName);
		Map<String, Object> result = new HashMap<String, Object>();
		if (table != null) {
			Item documentItem = table
					.getItem(new GetItemSpec().withPrimaryKey(DYNAMO_TABLE_ID, id));

			if (documentItem != null) {
				System.err.println("Loaded:");
				System.err.println(documentItem.toJSON());
				Map<String, Object> map = documentItem.asMap();
				return map;
//				for (Map.Entry<String, Object> entry : map.entrySet()) {
//					String json = documentItem.getJSON(entry.getKey());
//					//trim leading and trailing double quotes
//					json = json.replaceAll("^\"|\"$", "");
//					result.put(entry.getKey(), json);
//				}
			}
		}
		return result;
	}
	
	@Override
	@FaultIsolate(group = Group.DYNAMO, command = "selectForMultipleIds")
	public Map<String, Map<String, Object>> getForMultipleIdAndMultipleTables(Set<String> ids,
			Map<String, List<String>> tableColumns) {

		try {
			Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();
			
			TableKeysAndAttributes[] tkaa = new TableKeysAndAttributes[tableColumns.size()];
			PrimaryKey[] primaryKeys = new PrimaryKey[ids.size()];
			int i=0;
			for (String id : ids) {
				primaryKeys[i] = new PrimaryKey().addComponent(DYNAMO_TABLE_ID, id);
				i++;
			}
			i=0;
			for (Map.Entry<String, List<String>> tableColumn : tableColumns.entrySet()) {
				tkaa[i] = new TableKeysAndAttributes(tableColumn.getKey()).withPrimaryKeys(primaryKeys)
						.withAttributeNames(tableColumn.getValue());
				i++;
			}

			System.out.println("Making the request.");

			BatchGetItemOutcome outcome = dynamoDB.batchGetItem(tkaa);

			do {

				for (String tableName : outcome.getTableItems().keySet()) {
					System.out.println("Items in table " + tableName);
					List<Item> items = outcome.getTableItems().get(tableName);
					for (Item item : items) {
						if (result.containsKey(item.get(DYNAMO_TABLE_ID))) {
							result.get(item.get(DYNAMO_TABLE_ID)).put(tableName, item.toJSON());
						} else {
							Map<String, Object> tableResult = new HashMap<String, Object>();
							tableResult.put(tableName, item.asMap());//.toJSON());
							result.put(item.getString(DYNAMO_TABLE_ID), tableResult);							
						}
						System.out.println(item.toJSONPretty());
					}
				}

				// Check for unprocessed keys which could happen if you exceed
				// provisioned
				// throughput or reach the limit on response size.

				Map<String, KeysAndAttributes> unprocessedKeys = outcome.getUnprocessedKeys();

				if (outcome.getUnprocessedKeys().size() == 0) {
					System.out.println("No unprocessed keys found");
				} else {
					System.out.println("Retrieving the unprocessed keys");
					outcome = dynamoDB.batchGetItemUnprocessed(unprocessedKeys);
				}

			} while (outcome.getUnprocessedKeys().size() > 0);
			
			return result;
		} catch (Exception e) {
			System.err.println("Failed to retrieve items.");
			System.err.println(e.getMessage());
		}
		return null;

	}

	@Override
	public Map<String, Object> getMultipleIds(Set<String> ids, String table, List<String> attributes) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			attributes.add(DYNAMO_TABLE_ID);
			TableKeysAndAttributes tkaa = new TableKeysAndAttributes(table).withAttributeNames(attributes);
			PrimaryKey[] primaryKeys = new PrimaryKey[ids.size()];
			int i=0;
			for (String id : ids) {
				primaryKeys[i] = new PrimaryKey().addComponent(DYNAMO_TABLE_ID, id);
				i++;
			}
			tkaa.withPrimaryKeys(primaryKeys);
			BatchGetItemOutcome outcome = dynamoDB.batchGetItem(new BatchGetItemSpec().withTableKeyAndAttributes(tkaa));
			
			do {

				for (String tableName : outcome.getTableItems().keySet()) {
					System.out.println("Items in table " + tableName);
					List<Item> items = outcome.getTableItems().get(tableName);
					for (Item item : items) {
						//result.put(item.getString(DYNAMO_TABLE_ID), item.toJSON());
						result.put(item.getString(DYNAMO_TABLE_ID), item.asMap());
						System.out.println(item.toJSON());
					}
				}

				// Check for unprocessed keys which could happen if you exceed
				// provisioned
				// throughput or reach the limit on response size.

				Map<String, KeysAndAttributes> unprocessedKeys = outcome.getUnprocessedKeys();

				if (outcome.getUnprocessedKeys().size() == 0) {
					System.out.println("No unprocessed keys found");
				} else {
					System.out.println("Retrieving the unprocessed keys");
					outcome = dynamoDB.batchGetItemUnprocessed(unprocessedKeys);
				}

			} while (outcome.getUnprocessedKeys().size() > 0);

		} catch (Exception e) {
			System.err.println("Failed to retrieve items.");
			System.err.println(e.getMessage());
		}
		return result;
	}

	@Override
	public Map<String, Object> getFromMultipleTables(String id, Map<String, List<String>> tableColumns) {
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			
			TableKeysAndAttributes[] tkaa = new TableKeysAndAttributes[tableColumns.size()];
			int i = 0;
			for (Map.Entry<String, List<String>> tableColumn : tableColumns.entrySet()) {
				tableColumn.getValue().add(DYNAMO_TABLE_ID);
				tkaa[i] = new TableKeysAndAttributes(tableColumn.getKey()).addHashOnlyPrimaryKeys(DYNAMO_TABLE_ID, id)
						.withAttributeNames(tableColumn.getValue());
				i++;
			}
			System.out.println("Making the request.");

			BatchGetItemOutcome outcome = dynamoDB.batchGetItem(tkaa);

			do {

				for (String tableName : outcome.getTableItems().keySet()) {
					System.out.println("Items in table " + tableName);
					List<Item> items = outcome.getTableItems().get(tableName);
					for (Item item : items) {
						//result.put(tableName, item.toJSON());
						result.put(tableName, item.asMap());
						System.out.println(item.toJSON());
					}
				}

				// Check for unprocessed keys which could happen if you exceed
				// provisioned
				// throughput or reach the limit on response size.

				Map<String, KeysAndAttributes> unprocessedKeys = outcome.getUnprocessedKeys();

				if (outcome.getUnprocessedKeys().size() == 0) {
					System.out.println("No unprocessed keys found");
				} else {
					System.out.println("Retrieving the unprocessed keys");
					outcome = dynamoDB.batchGetItemUnprocessed(unprocessedKeys);
				}

			} while (outcome.getUnprocessedKeys().size() > 0);

			return result;
		} catch (Exception e) {
			System.err.println("Failed to retrieve items.");
			System.err.println(e.getMessage());
		}
		return null;
	}

	@Override
	public void updateAttributes(String tableName, String id, Map<String, Object> pathValues) {
		Table table = dynamoDB.getTable(tableName);
		
		Map<String, String> expressionAttributeNames = new HashMap<String, String>();
		Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
		StringBuilder expression = new StringBuilder();
		expression.append("SET ");
		boolean comma = false;
		int i = 1;
		for (Map.Entry<String, Object> entry : pathValues.entrySet()) {

			if (comma) {
				expression.append(", ");				
			} else {
				comma = true;				
			}

			
			if (entry.getKey().contains(".")) {
				String[] parts = entry.getKey().trim().split("\\.");
				List<String> path = new ArrayList<String>();
				for (int p=0;p<parts.length;p++) {
					expressionAttributeNames.put("#" + parts[p], parts[p]);
					path.add("#"+parts[p]);
				}
				expression.append(StringUtils.join(path, ".")).append(" = ").append(":").append(i);
			} else {
				expressionAttributeNames.put("#"+entry.getKey(), entry.getKey());				
				expression.append("#").append(entry.getKey()).append(" = ").append(":").append(i);				
			}
			//check if value is json, if so store as dynamodb json
//			boolean added = false;
//			if (entry.getValue() instanceof String) {
//				try {
//					Item item = Item.fromJSON((String)entry.getValue());
//					expressionAttributeValues.put(":"+i, item);
//					added = true;
//				} catch (Exception e) {
//					System.err.println("exception caught");
//					//e.printStackTrace();
//				}
//			}
//			if (!added) {
				expressionAttributeValues.put(":"+i, entry.getValue());				
//			}
			
			i++;
		}
		UpdateItemOutcome outcome =  table.updateItem(
		    DYNAMO_TABLE_ID,          // key attribute name
		    id,           // key attribute value
		    expression.toString(),//"add #A :val1 set #P = #P - :val2 remove #I", // UpdateExpression
		    expressionAttributeNames,
		    expressionAttributeValues);
	}
	
	@Override
	public void updateAttribute(String tableName, String id, String path, Object value) {
		Map<String, Object> attributeValues = new HashMap<String, Object>();
		attributeValues.put(path, value);
		updateAttributes(tableName, id, attributeValues);
	}

	
	@Override
	public void update(String tableName, String id, String json) {
		Table table = dynamoDB.getTable(tableName);
		Item jsonItem = Item.fromJSON(json);
		jsonItem.withPrimaryKey(DYNAMO_TABLE_ID, id);
		table.putItem(jsonItem);
	}
	
	public Map<String, Object> getMultipleIdsAll(Set<String> ids, String table) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			TableKeysAndAttributes tkaa = new TableKeysAndAttributes(table);
			PrimaryKey[] primaryKeys = new PrimaryKey[ids.size()];
			int i=0;
			for (String id : ids) {
				primaryKeys[i] = new PrimaryKey().addComponent(DYNAMO_TABLE_ID, id);
				i++;
			}
			tkaa.withPrimaryKeys(primaryKeys);
			BatchGetItemOutcome outcome = dynamoDB.batchGetItem(new BatchGetItemSpec().withTableKeyAndAttributes(tkaa));
			
			do {

				for (String tableName : outcome.getTableItems().keySet()) {
					System.out.println("Items in table " + tableName);
					List<Item> items = outcome.getTableItems().get(tableName);
					for (Item item : items) {
						result.put(item.getString(DYNAMO_TABLE_ID), item.asMap());
						System.out.println(item.toJSON());
					}
				}

				// Check for unprocessed keys which could happen if you exceed
				// provisioned
				// throughput or reach the limit on response size.

				Map<String, KeysAndAttributes> unprocessedKeys = outcome.getUnprocessedKeys();

				if (outcome.getUnprocessedKeys().size() == 0) {
					System.out.println("No unprocessed keys found");
				} else {
					System.out.println("Retrieving the unprocessed keys");
					outcome = dynamoDB.batchGetItemUnprocessed(unprocessedKeys);
				}

			} while (outcome.getUnprocessedKeys().size() > 0);

		} catch (Exception e) {
			System.err.println("Failed to retrieve items.");
			System.err.println(e.getMessage());
		}
		return result;

	}		
	
	public Map<String, Map<String, Object>> getForMultipleIdAndMultipleTablesAll(Set<String> ids, List<String> tables) {
		try {
			Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();
			
			TableKeysAndAttributes[] tkaa = new TableKeysAndAttributes[tables.size()];
			PrimaryKey[] primaryKeys = new PrimaryKey[ids.size()];
			int i=0;
			for (String id : ids) {
				primaryKeys[i] = new PrimaryKey().addComponent(DYNAMO_TABLE_ID, id);
				i++;
			}
			i=0;
			for (String table : tables) {
				tkaa[i] = new TableKeysAndAttributes(table).withPrimaryKeys(primaryKeys);
				i++;
			}

			System.out.println("Making the request.");

			BatchGetItemOutcome outcome = dynamoDB.batchGetItem(tkaa);

			do {

				for (String tableName : outcome.getTableItems().keySet()) {
					System.out.println("Items in table " + tableName);
					List<Item> items = outcome.getTableItems().get(tableName);
					for (Item item : items) {
						if (result.containsKey(item.get(DYNAMO_TABLE_ID))) {
							result.get(item.get(DYNAMO_TABLE_ID)).put(tableName, item.toJSON());
						} else {
							Map<String, Object> tableResult = new HashMap<String, Object>();
							tableResult.put(tableName, item.asMap());
							result.put(item.getString(DYNAMO_TABLE_ID), tableResult);							
						}
						System.out.println(item.toJSONPretty());
					}
				}

				// Check for unprocessed keys which could happen if you exceed
				// provisioned
				// throughput or reach the limit on response size.

				Map<String, KeysAndAttributes> unprocessedKeys = outcome.getUnprocessedKeys();

				if (outcome.getUnprocessedKeys().size() == 0) {
					System.out.println("No unprocessed keys found");
				} else {
					System.out.println("Retrieving the unprocessed keys");
					outcome = dynamoDB.batchGetItemUnprocessed(unprocessedKeys);
				}

			} while (outcome.getUnprocessedKeys().size() > 0);
			
			return result;
		} catch (Exception e) {
			System.err.println("Failed to retrieve items.");
			System.err.println(e.getMessage());
		}
		return null;

	}
		
	public Map<String, Object> getFromMultipleTablesAll(String id, List<String> tables) {
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			
			TableKeysAndAttributes[] tkaa = new TableKeysAndAttributes[tables.size()];
			int i = 0;
			for (String table : tables) {
				tkaa[i] = new TableKeysAndAttributes(table).addHashOnlyPrimaryKeys(DYNAMO_TABLE_ID, id);
				i++;
			}
			System.out.println("Making the request.");

			BatchGetItemOutcome outcome = dynamoDB.batchGetItem(tkaa);

			do {

				for (String tableName : outcome.getTableItems().keySet()) {
					System.out.println("Items in table " + tableName);
					List<Item> items = outcome.getTableItems().get(tableName);
					for (Item item : items) {
						result.put(tableName, item.asMap());
						System.out.println(item.toJSONPretty());
					}
				}

				// Check for unprocessed keys which could happen if you exceed
				// provisioned
				// throughput or reach the limit on response size.

				Map<String, KeysAndAttributes> unprocessedKeys = outcome.getUnprocessedKeys();

				if (outcome.getUnprocessedKeys().size() == 0) {
					System.out.println("No unprocessed keys found");
				} else {
					System.out.println("Retrieving the unprocessed keys");
					outcome = dynamoDB.batchGetItemUnprocessed(unprocessedKeys);
				}

			} while (outcome.getUnprocessedKeys().size() > 0);

			return result;
		} catch (Exception e) {
			System.err.println("Failed to retrieve items.");
			System.err.println(e.getMessage());
		}
		return null;

	}


	@Override
	public void delete(String table, String id) {
	}

	@Override
	public void deleteAttributes(String table, String id, String... columns) {
	}
	
	
	public void createItems(String tableName, String id, Map<String, String> columns) {

        Table table = dynamoDB.getTable(tableName);
        try {

            Item item = new Item()
                .withPrimaryKey(DYNAMO_TABLE_ID, id);
            for (Map.Entry<String, String> entry : columns.entrySet()) {
            	item.withJSON(entry.getKey(), entry.getValue());
            }
            table.putItem(item);


        } catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());

        }
    }

    private void retrieveItem(String tableName, String id, String ... columns) {
        Table table = dynamoDB.getTable(tableName);

        try {

            Item item = table.getItem(DYNAMO_TABLE_ID, id,  StringUtils.join(columns,","), null);

            System.out.println("Printing item after retrieving it....");
            System.out.println(item.toJSONPretty());

        } catch (Exception e) {
            System.err.println("GetItem failed.");
            System.err.println(e.getMessage());
        }   

    }

    public void updateAddNewAttribute(String tableName, String id, String column, String value) {
        Table table = dynamoDB.getTable(tableName);

        try {

            Map<String, String> expressionAttributeNames = new HashMap<String, String>();
            expressionAttributeNames.put("#na", "NewAttribute");

            UpdateItemSpec updateItemSpec = new UpdateItemSpec()
            .withPrimaryKey(DYNAMO_TABLE_ID, id)
            .withUpdateExpression("set #na = :val1")
            .withNameMap(new NameMap()
                .with("#na", column))
            .withValueMap(new ValueMap()
                .withString(":val1", value))
            .withReturnValues(ReturnValue.ALL_NEW);

            UpdateItemOutcome outcome =  table.updateItem(updateItemSpec);

            // Check the response.
            System.out.println("Printing item after adding new attribute...");
            System.out.println(outcome.getItem().toJSONPretty());           

        }   catch (Exception e) {
            System.err.println("Failed to add new attribute in " + tableName);
            System.err.println(e.getMessage());
        }        
    }

    //private void writeMultipleItemsBatchWrite(String tableName, String id, Map<String, String> columnValues) {
    private void writeMultipleItemsBatchWrite(String tableName, String id, Map<String, String> columnValues) {
    	        try {                    
    	        	ItemValueConformer valueConformer = new ItemValueConformer();

    	        	Table table = dynamoDB.getTable(tableName);

    	        	StringBuilder updateExpression = new StringBuilder();
    	        	updateExpression.append("SET ");
    	        	Map<String, String> expressionAttributeNames = new HashMap<String, String>();
    	        	Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
    	        	for (Map.Entry<String, String> attribute : columnValues.entrySet()) {
    	        		Map newJson = (Map)valueConformer.transform(Jackson.fromJsonString(attribute.getValue(), Map.class));
    	        		System.err.println("NEW JSON:" + newJson);
        	        	expressionAttributeNames.put("#"+attribute.getKey(), attribute.getKey());    	        		
        	        	expressionAttributeValues.put(":"+attribute.getKey(), attribute.getValue());
        	        	updateExpression.append("#").append(attribute.getKey()).append(" :").append(attribute.getKey()).append(" ");
    	        	}

    	        	
    	        	System.err.println("UPDATE EXPRESSION:" + updateExpression);
    	        	UpdateItemOutcome outcome =  table.updateItem(
    	        	    DYNAMO_TABLE_ID,          // key attribute name
    	        	    id,           // key attribute value
    	        	    //"add #A :val1 set #P = #P - :val2 remove #I", // UpdateExpression
    	        	    updateExpression.toString(),
    	        	    expressionAttributeNames,
    	        	    expressionAttributeValues);
        	
//            // Add a new item to Forum
//            TableWriteItems tableWriteItems = new TableWriteItems(tableName);
//            Item item = new Item()
//                    .withPrimaryKey(DYNAMO_TABLE_ID, id);
//            for (Map.Entry<String, String> entry : columnValues.entrySet()) {
//            	item.withJSON(entry.getKey(), entry.getValue());
//            }
//            tableWriteItems.withItemsToPut(item);
//
//            System.out.println("Making the request.");
//            BatchWriteItemOutcome outcome = dynamoDB..batchWriteItem(tableWriteItems);

//            do {
//
//                // Check for unprocessed keys which could happen if you exceed provisioned throughput
//
//                Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();
//
//                if (outcome.getUnprocessedItems().size() == 0) {
//                    System.out.println("No unprocessed items found");
//                } else {
//                    System.out.println("Retrieving the unprocessed items");
//                    outcome = dynamoDB.batchWriteItemUnprocessed(unprocessedItems);
//                }
//
//            } while (outcome.getUnprocessedItems().size() > 0);

        }  catch (Exception e) {
            System.err.println("Failed to retrieve items: ");
            e.printStackTrace(System.err);
        }  

    }


    
//    private void updateMultipleAttributes(String tableName, String id, Map<String, String> columnValues) {
//
//        Table table = dynamoDB.getTable(tableName);
//
//        try {
//        	StringBuilder expression = new StringBuilder();
//        	expression.append("SET ")
//           UpdateItemSpec updateItemSpec = new UpdateItemSpec()
//            .withPrimaryKey(DYNAMO_TABLE_ID, id)
//            .withUpdateExpression("add #a :val1 set #na=:val2")
//            .withNameMap(new NameMap()
//                .with("#a", "Authors")
//                .with("#na", "NewAttribute"))
//            .withValueMap(new ValueMap()
//                .withStringSet(":val1", "Author YY", "Author ZZ")
//                .withString(":val2", "someValue"))
//            .withReturnValues(ReturnValue.ALL_NEW);
//
//            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
//
//            // Check the response.
//            System.out
//            .println("Printing item after multiple attribute update...");
//            System.out.println(outcome.getItem().toJSONPretty());
//
//        } catch (Exception e) {
//            System.err.println("Failed to update multiple attributes in "
//                    + tableName);
//            System.err.println(e.getMessage());
//
//        }
//    }

    public void updateExistingAttributeConditionally(String tableName, String id, String column, String newValue, String oldValue) {

        Table table = dynamoDB.getTable(tableName);

        try {

            // Specify the desired price (25.00) and also the condition (price =
            // 20.00)

            UpdateItemSpec updateItemSpec = new UpdateItemSpec()
            .withPrimaryKey(DYNAMO_TABLE_ID, id)
            .withReturnValues(ReturnValue.ALL_NEW)
            .withUpdateExpression("set #p = :val1")
            .withConditionExpression("#p = :val2")
            .withNameMap(new NameMap()
                .with("#p", column))
            .withValueMap(new ValueMap()
                .withString(":val1", newValue)
                .withString(":val2", oldValue));

            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);

            // Check the response.
            System.out
            .println("Printing item after conditional update to new attribute...");
            System.out.println(outcome.getItem().toJSONPretty());

        } catch (Exception e) {
            System.err.println("Error updating item in " + tableName);
            System.err.println(e.getMessage());
        }
    }

    public void deleteItemConditional(String tableName, String id, String column, String currentValue) {

        Table table = dynamoDB.getTable(tableName);

        try {

            DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
            .withPrimaryKey(DYNAMO_TABLE_ID, id)
            .withConditionExpression("#ip = :val")
            .withNameMap(new NameMap()
                .with("#ip", column))
            .withValueMap(new ValueMap()
            .withString(":val", currentValue))
            .withReturnValues(ReturnValue.ALL_OLD);

            DeleteItemOutcome outcome = table.deleteItem(deleteItemSpec);

            // Check the response.
            System.out.println("Printing item that was deleted...");
            System.out.println(outcome.getItem().toJSONPretty());

        } catch (Exception e) {
            System.err.println("Error deleting item in " + tableName);
            System.err.println(e.getMessage());
        }
    }

}
