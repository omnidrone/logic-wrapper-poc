package net.omnidrone.logic.domain;

import java.util.HashMap;
import java.util.Map;

public class Spaceship {
	public String id;
	public String name;
	public int velocity;
	public String cargo;
	public int crewMembers;
	
	public Map<String, Object> asMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("name", name);
		map.put("velocity", velocity);
		map.put("cargo", cargo);
		map.put("crewMembers", crewMembers);
		return map;
	}
}
