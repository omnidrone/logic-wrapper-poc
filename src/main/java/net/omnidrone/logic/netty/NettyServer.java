package net.omnidrone.logic.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.SocketAddress;

import net.omnidrone.savage.guice.Lifecycle;

import com.google.common.util.concurrent.AbstractIdleService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class NettyServer extends AbstractIdleService implements Lifecycle {	
	private final SocketAddress address;
	private final ServerBootstrap bootstrap;
	
	EventLoopGroup bossGroup = new NioEventLoopGroup(1); // (1)
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    
	
    @Inject
	NettyServer(SocketAddress address) {
		this.bootstrap = new ServerBootstrap();
		this.address = address;
        bootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class);
	}
 
	@Override
	protected void startUp() throws Exception {
		bootstrap.childHandler(new LogicChannelHandler());
		bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
		bootstrap.bind(address);
	}
 
	@Override
	protected void shutDown() throws Exception {
		workerGroup.shutdownGracefully();
		bossGroup.shutdownGracefully();
	}

	@Override
	public void onStartUp() {
		this.startAsync();
	}

	@Override
	public void onShutdown() {
		this.stopAsync();
	}
}