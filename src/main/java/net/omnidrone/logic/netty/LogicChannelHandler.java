package net.omnidrone.logic.netty;

import com.google.protobuf.ExtensionRegistry;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import net.omnidrone.game.protobuf.GameProtos;

public class LogicChannelHandler extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ExtensionRegistry registry = ExtensionRegistry.newInstance();
		GameProtos.registerAllExtensions(registry);
		ChannelPipeline pipeline = ch.pipeline();
		//pipeline.addLast(new LengthFieldBasedFrameDecoder(65536, 0, 2, 0, 2));
        pipeline.addLast(new ProtobufVarint32FrameDecoder());

		pipeline.addLast(new ProtobufDecoder(GameProtos.GameCommand.getDefaultInstance(), registry));
		//pipeline.addLast(new LengthFieldPrepender(2));
		pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
		pipeline.addLast(new ProtobufEncoder());
		//pipeline.addLast(new CommandHandler());
		pipeline.addLast(new SessionHandler());
		pipeline.addLast(new SessionOutboundHandlerAdapter());
		//pipeline.addLast(new IdleStateHandler(10, 10, 0));
		//pipeline.addLast(new IdleHandler());
		
	}
	
	public class IdleHandler extends ChannelDuplexHandler {
	     @Override
	     public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
	         if (evt instanceof IdleStateEvent) {
	             IdleStateEvent e = (IdleStateEvent) evt;
	             if (e.state() == IdleState.READER_IDLE) {
	                 System.err.println("Reader has been idle!");
	            	 //ctx.close();
	             } else if (e.state() == IdleState.WRITER_IDLE) {
	                 System.err.println("Writer has been idle!");
	                 //ctx.writeAndFlush(new PingMessage());
	             }
	         }
	     }
	 }

}
