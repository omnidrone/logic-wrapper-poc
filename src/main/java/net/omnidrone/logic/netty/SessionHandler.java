package net.omnidrone.logic.netty;

import com.google.protobuf.Any;
import com.google.protobuf.ExtensionRegistry;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.omnidrone.game.protobuf.GameProtos;
import net.omnidrone.game.protobuf.GameProtos.GameCommand;
import net.omnidrone.game.protobuf.GameProtos.Login;
import net.omnidrone.logic.InjectorHelper;
import net.omnidrone.logic.account.Account;
import net.omnidrone.logic.session.rpc.SessionRPC;
public class SessionHandler extends SimpleChannelInboundHandler<GameCommand> {

	private String userId = "abc123";
	
	ExtensionRegistry registry = ExtensionRegistry.newInstance();
	
	private boolean noLogin = true; //for testing we dont care about login..

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, GameCommand msg) throws Exception {
		System.err.println("bytes read:" + msg);
		SessionRPC session = InjectorHelper.injector.getInstance(SessionRPC.class);
		//if login message -> authorize and invalidate other sessions if any
		if (session == null) {
			if (noLogin) {
				//session = createSession(msg.getUserId(), ctx);
				session.onMessage(userId, ctx, msg);
			} else {
				System.err.println("No session, must be a login command");
				Any any = msg.getCommand();
				if (any.is(GameProtos.Login.class)) {
					
				Login login = any.unpack(GameProtos.Login.class);
				String externalId = login.getExternalId();
				System.err.println("ExternalId:"+externalId);
				Account account = InjectorHelper.injector.getInstance(Account.class);
				userId = account.getUserId(externalId);
				GameProtos.LoginResponse.Builder lrb = GameProtos.LoginResponse.newBuilder();
				lrb.setUserId(userId);
				System.err.println("send back " + userId);
				GameProtos.GameCommandResponse.Builder builder = GameProtos.GameCommandResponse.newBuilder();
				builder.setCommand(Any.pack(lrb.build()));

				ctx.writeAndFlush(builder.build());
				}
			}
		} else {
			System.err.println("session found, just dispatch message");

			if (isHandledByJava(msg)) {
				
			} else {
				session.onMessage(userId, ctx, msg);			
			}

		}
		

	}
		
	private boolean isHandledByJava(GameCommand gameCommand) {
		return false;
	}

	/*
	private Session createSession(String userId, ChannelHandlerContext ctx) {
		
		GameProtos.registerAllExtensions(registry);		

		JsonPersistence persistence = InjectorHelper.injector.getInstance(JsonPersistence.class);


		MessageCallback messageCallback = new MessageCallback() {

			@Override
			public void send(byte[] data) {
				System.err.println("send back bytes to client");
				try {
					ctx.writeAndFlush(GameProtos.GameCommandResponse.parseFrom(data, registry));
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//ctx.writeAndFlush(data);
			}
		};

		//Session session = new SessionJava(registry);
		//Session session = new SessionJNAJson();
		
		System.err.println("Session created call init!");
		session.init(userId, persistence, messageCallback);
		return session;

	}
	*/
}
