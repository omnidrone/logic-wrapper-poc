package net.omnidrone.logic.netty;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.sun.jna.Native;

import net.omnidrone.logic.InjectorHelper;
import net.omnidrone.logic.account.Account;
import net.omnidrone.logic.persistence.DynamoDBJsonPersistence;
import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.logic.services.DbApiService;
import net.omnidrone.logic.session.MessageCallback;
import net.omnidrone.logic.session.Session;
import net.omnidrone.logic.session.SessionJNAAvro;
import net.omnidrone.logic.session.SessionJNAJson;
import net.omnidrone.logic.session.rpc.SessionRPC;
import net.omnidrone.logic.session.rpc.SessionRPCImpl;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBConfiguration;
import net.omnidrone.savage.persistence.dynamodb.DynamoDBLifecycle;
import net.omnidrone.savage.persistence.redis.RedisNode;
import net.omnidrone.savage.persistence.redis.RedisNodeConfiguration;
import net.omnidrone.savage.persistence.redis.RedisNodeFactory;


public class NettyModule extends AbstractModule {
	private static final Logger logger = LoggerFactory.getLogger(NettyModule.class);
	
	@Override
	protected void configure() {
		logger.info("bind netty server..");
		//System.setProperty("jna.encoding", "UTF8");
		System.setProperty("LD_LIBRARY_PATH", "/usr/local/lib");

		bind(NettyServer.class).asEagerSingleton();
		DynamoDBConfiguration config = new DynamoDBConfiguration();
		config.accessKey = "AKIAIMGAFWX3NIUUWMBA";
		config.secretKey = "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";
		config.dynamoTestDb = false;
		config.regionName = "eu-west-1";
		bind(DynamoDBConfiguration.class).toInstance(config);
		bind(DynamoDBLifecycle.class);
		bind(JsonPersistence.class).to(DynamoDBJsonPersistence.class);
		bind(Account.class);
		bind(RedisNodeFactory.class);
		bind(DbApiService.class);
		SessionRPC session = new SessionRPCImpl("localhost", 50051);
		bind(SessionRPC.class).toInstance(session);
	}
	
	@Provides
	SocketAddress provideSocketAddress() {
		return new InetSocketAddress(8081);
	}
	
	public static void main(String[] args) {
		Injector injector = Guice.createInjector(new NettyModule());
		InjectorHelper.injector = injector;
		
		final DbApiService dbService = injector.getInstance(DbApiService.class);
		try {
			dbService.start();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		final NettyServer server = injector.getInstance(NettyServer.class);
		server.startAsync();
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run() {
				server.stopAsync();
			}			
		});
		
		//Session session = new SessionRPC("localhost", 50051);
		//session.init("acb123", null, null);
		/*
		//load c lib
		try {
		Session session = new SessionJNAAvro();
		JsonPersistence persistence = InjectorHelper.injector.getInstance(JsonPersistence.class);
		
		session.init("0", persistence, new MessageCallback() {
			@Override
			public void send(byte[] data) {
			}});
		} catch (Error e) {
			e.printStackTrace();
		}
		*/		
	}
}