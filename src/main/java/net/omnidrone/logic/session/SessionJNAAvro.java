package net.omnidrone.logic.session;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonEncoder;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.PointerByReference;

import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.logic.session.SessionJNA.CSessionLibrary.KeyStruct;
import net.omnidrone.logic.session.SessionJNAAvro.CSessionLibrary.AvroBytes;

public class SessionJNAAvro implements Session {

	AvroBytes ref;
	JsonHelper jsonHelper = new JsonHelper();
	ObjectMapper mapper = new ObjectMapper();
	CSessionLibrary.Functions funcs = new CSessionLibrary.Functions();
	static CSessionLibrary clib;
	static  {
		clib = (CSessionLibrary) Native.loadLibrary("c_wrapper-d", CSessionLibrary.class);		
	}

	String userId;
	Pointer self;
		
	public interface CSessionLibrary extends Library {
		
		public static class IdStruct extends Structure {
			public static class ByValue extends KeyStruct implements Structure.ByValue {
			}
			public static class ByReference extends KeyStruct implements Structure.ByReference {
			}

			public String table;
			public String id;
			public String column;

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("table", "id", "column");
			}
		}
		
		// Equivalent JNA mapping
		class AvroBytes extends Structure {
		    public static class ByReference extends AvroBytes implements Structure.ByReference { }
		    public static class ByValue extends AvroBytes implements Structure.ByValue { }
		    public int dataLength;
		    public Pointer data;
		    
		    @Override
			protected List getFieldOrder() {
				return Arrays.asList("dataLength", "data");
			}
		}
				
		public class Functions extends Structure {

			//Map<String, Object> get(String table, String id);
			public static interface get extends Callback {
				String invoke(String table, String id);
			}
			
			public static interface getAvro extends Callback {
				void invoke(String table, String id, Pointer vals, int noVals);
			}
			
			//Map<String, Object> getAttributes(String table, String id, List<String> attributes);
			public static interface getAttribute extends Callback {
				String invoke(String table, String id, String attribute);
			}
			
			public static interface getAttributes extends Callback {
				String invoke(String table, String id, Pointer attributes, int noAttributes);
			}
			//Map<String, Object> getMultipleIds(Set<String> ids, String table, List<String> attributes);		
			//Map<String, Map<String, Object>> getForMultipleIdAndMultipleTables(Set<String> ids, Map<String, List<String>> tableAttributes);
			//Map<String, Object> getFromMultipleTables(String id, Map<String, List<String>> tableAttributes);
			//Map<String, Object> getMultipleIdsAll(Set<String> ids, String table);		
			//Map<String, Map<String, Object>> getForMultipleIdAndMultipleTablesAll(Set<String> ids, List<String> tables);
			//Map<String, Object> getFromMultipleTablesAll(String id, List<String> tables);
			//void update(String table, String json);
			//void update(String table, String id, String json);
			
			public static interface update extends Callback {
				void invoke(String table, String id, String json);
			}

			public static interface updateAvro extends Callback {
				void invoke(String table, String id, Pointer updateBytes, int noBytes);
			}

			//void updateAttribute(String table, String id, String attribute, Object value);
			public static interface updateAttributes extends Callback {
				void invoke(String table, String id, String json);
			}

			//void updateAttributes(String table, String id, Map<String, Object> attributeValues);
			//void delete(String table, String id);
			
			//void deleteAttributes(String table, String id, String ... columns);
			
			public static interface flushData extends Callback {
				void invoke();
			}

			public static interface sendMessage extends Callback {
				void invoke(Pointer bytes, int length);
			}

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("get", "getAvro", "getAttribute", "getAttributes", "update", "updateAvro", "updateAttributes", "flushData", "sendMessage");
			}

			public get get;
			public getAvro getAvro;
			public getAttribute getAttribute;
			public getAttributes getAttributes;
			public update update;
			public updateAvro updateAvro;
			public updateAttributes updateAttributes;
			public flushData flushData;
			public sendMessage sendMessage;

		}

		public Pointer session_ctor(String userId);
		
		public int session_init(Pointer session, Functions.get get, Functions.getAvro getAvro, Functions.getAttribute getAttribute, Functions.getAttributes getAttributes, Functions.update update,
				Functions.updateAvro updateAvro, Functions.updateAttributes updateAttributes, Functions.flushData flushData, Functions.sendMessage sendMessage);

		public void session_onMessage(Pointer session, Pointer bytes, int numBytes);
		
	}

	@Override
	public boolean init(String userId, JsonPersistence jsonPersistence, MessageCallback messageCallback) {
		this.userId = userId;
		funcs.get = new CSessionLibrary.Functions.get() {
			
			@Override
			public String invoke(String table, String id) {
				System.err.println("get invoked from C");
				Map<String, Object> map = jsonPersistence.get(table, id);
				System.err.println("REMOVE Id from map");
				System.err.println(map.remove("Id"));
				try {
					return mapper.writeValueAsString(map);
				} catch (Exception e) {
					e.printStackTrace();
					return returnError(1, e.getMessage());
				}
			}
		};
		funcs.getAvro = new CSessionLibrary.Functions.getAvro() {
			
			@Override
			public void invoke(String table, String id, Pointer vals, int noVals) {
				System.err.println("get avro invoked from C");
				System.err.println("table:" + table + ", id:" + id);
				Map<String, Object> map = jsonPersistence.get(table, id);
				System.err.println("REMOVE Id from map");
				System.err.println(map.remove("Id"));
				try {
					String json = mapper.writeValueAsString(map);
					System.err.println("transform json to avro object");
					System.err.println(json);
					//add null terminator
					Schema schema = new Schema.Parser().parse(new File("/home/henrik/development/logic-wrapper-poc/avro/order.avsc"));
					byte[] bytes = jsonToAvro(json, schema);
					//byte[] prefixLength = ByteBuffer.allocate(4).putInt(bytes.length).array();
					//byte[] full = ByteBuffer.allocate(4 + bytes.length).put(ByteBuffer.allocate(4).putInt(bytes.length).array()).put(bytes).array();
					//nullBytes[bytes.length] = '\0';
					//test bytes
					DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
					DecoderFactory decoderFactory = new DecoderFactory();
					
					Decoder decoder = decoderFactory.binaryDecoder(bytes, 0, bytes.length, null);
					
					GenericRecord order = datumReader.read(null, decoder);
					System.err.println(order);
					
					System.err.println("Return bytes to C");
					
					//return new String(bytes);
//					System.err.println("bytes:" + bytes.length);
//					System.err.println("bytes:" + bytes);
//					AvroBytes ab = new AvroBytes();
//					ab.data = bytes;
//					ab.size = bytes.length;
					Pointer p = new Memory(bytes.length * Native.getNativeSize(Byte.class));
					for (int i = 0; i < bytes.length; i++) {
						p.setByte(i, bytes[i]);
					}
					System.err.println("Bytes length: " + bytes.length);
					vals = p;
					noVals = bytes.length;
					//return new String(bytes);
//					AvroBytes ab = new AvroBytes();
//					ab.dataLength = bytes.length;
//					ab.data = p;
//					ref = ab;
//					return ab;
//					pbr.setValue(p);
//					Memory m = new Memory();
//					return ByteBuffer.wrap(bytes);
				} catch (Exception e) {
					e.printStackTrace();
					//return returnError(1, e.getMessage());
					noVals = 0;
				}
			}
		};
		funcs.getAttribute = new CSessionLibrary.Functions.getAttribute() {
			
			@Override
			public String invoke(String table, String id, String attribute) {
				System.err.println("getAttributes invoked from C");
				System.err.println("table:" + table);
				System.err.println("id:" + id);
				System.err.println("attribute:" + attribute);
				Map<String, Object> map = jsonPersistence.getAttribute(table, id, attribute);
				
				try {
					return mapper.writeValueAsString(map);
				} catch (Exception e) {
					e.printStackTrace();
					return returnError(1, e.getMessage());
				}
			}
		};

		funcs.getAttributes = new CSessionLibrary.Functions.getAttributes() {
			
			@Override
			public String invoke(String table, String id, Pointer attributesPtr, int noAttributes) {
				System.err.println("getAttributes invoked from C");
				System.err.println("table:" + table);
				System.err.println("id:" + id);
				System.err.println("attributes:" + attributesPtr);
				// getStringArray copies the contents of the C-allocated memory buffer into a Java-managed String[]
				String[] attributes = attributesPtr.getStringArray(0, noAttributes);
				System.err.println("attributes:" + attributes);
				for (String a : attributes) {
					System.err.println(a);
				}
				Map<String, Object> map = jsonPersistence.getAttributes(table, id, Arrays.asList(attributes));
				
				try {
					return mapper.writeValueAsString(map);
				} catch (Exception e) {
					e.printStackTrace();
					return returnError(1, e.getMessage());
				}
			}
		};
		funcs.update = new CSessionLibrary.Functions.update() {

			@Override
			public void invoke(String table, String id, String json) {
				System.err.println("plain update not implemented");
				
			}
		
		};
		
		funcs.updateAvro = new CSessionLibrary.Functions.updateAvro() {
			
			@Override
			public void invoke(String table, String id, Pointer updateBytes, int noBytes) {
				byte[] bytes = updateBytes.getByteArray(0, noBytes);
				System.err.println("decode order, no bytes:" + noBytes);
				try {
				Schema schema = new Schema.Parser().parse(new File("/home/henrik/development/logic-wrapper-poc/avro/order.avsc"));
				DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
				DecoderFactory decoderFactory = new DecoderFactory();
				
				Decoder decoder = decoderFactory.binaryDecoder(bytes, 0, noBytes, null);
				
				GenericRecord order = datumReader.read(null, decoder);
				System.err.println(order);
//				DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, datumReader);
//				GenericRecord user = null;
//				while (dataFileReader.hasNext()) {
//				// Reuse user object by passing it to next(). This saves us from
//				// allocating and garbage collecting many objects for files with
//				// many items.
//				user = dataFileReader.next(user);
//				System.out.println(user);
//				}
				
				//mapper.setDeserializationConfig(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
				//mapper = mapper.setSerializationInclusion(Include.NON_NULL);
				//mapper.configure(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, false);
				//mapper.configure(Feature.WRITE_NULL_PROPERTIES, false);
				//mapper.configure(Feature.WRITE_EMPTY_JSON_ARRAYS, false);
				//mapper.read
				//jsonPersistence.update(table, id, json);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		
		funcs.updateAttributes = new CSessionLibrary.Functions.updateAttributes() {
			
			@Override
			public void invoke(String table, String id, String json) {
				byte[] bytes = json.getBytes();
				int noBytes = bytes.length;
				System.err.println("decode order, no bytes:" + noBytes);
				try {
				Schema schema = new Schema.Parser().parse(new File("/home/henrik/development/logic-wrapper-poc/avro/order.avsc"));
				DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
				DecoderFactory decoderFactory = new DecoderFactory();
				
				Decoder decoder = decoderFactory.binaryDecoder(bytes, 0, noBytes, null);
				
				GenericRecord order = datumReader.read(null, decoder);
				System.err.println(order);
				
				System.err.println("updateAttributes invoked from C");
				Map<String, Object> attributeValues;
			
					attributeValues = mapper.readValue(json, Map.class);
					jsonPersistence.updateAttributes(table, userId, attributeValues);;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		funcs.flushData = new CSessionLibrary.Functions.flushData() {

			@Override
			public void invoke() {
				System.err.println("Flush data invoked from C");
				//persistenceHandle.flushData();
			}

		};
		
		funcs.sendMessage = new CSessionLibrary.Functions.sendMessage() {

			@Override
			public void invoke(Pointer bytes, int length) {
				System.err.println("Send message invoked from C");
				System.err.println("message callback length:" + length);
				System.err.println("message callback:" + bytes.getByteArray(0, length));
				messageCallback.send(bytes.getByteArray(0, length));
			}
		};

		System.err.println("Init C session");
		// call the C function
		Pointer self = clib.session_ctor(userId);
		clib.session_init(self, funcs.get, funcs.getAvro, funcs.getAttribute, funcs.getAttributes, funcs.update, funcs.updateAvro, funcs.updateAttributes, funcs.flushData, funcs.sendMessage);
		this.self = self;
		return true;
	}

	@Override
	public void onMessage(byte[] data) {
		// convert byte array
		// Memory allocates a contiguous block of memory suitable for sharing
		// with C directly - no copying is necessary
		Pointer bytesRef = new Memory(data.length);
		// note: Memory instance (and its associated memory allocation) will be
		// freed when ex8p goes out of scope
		for (int bloop = 0; bloop < data.length; bloop++) {
			// populate the array with junk data (just for the sake of the
			// example)
			bytesRef.setByte(bloop, data[bloop]);
		}
		System.err.println("OnMessage C session");

		clib.session_onMessage(this.self, bytesRef, data.length);
		
		System.err.println("OnMessage C session finished executing");
	}
	
	public String getUserId() {
		return userId;
	}
	
	private String returnError(int code, String message) {
		Map<String, String> json = new HashMap<String, String>();
		json.put("code", ""+code);
		json.put("message", message);
		return jsonHelper.toJson(json);
	}
	
	public static byte[] jsonToAvro(String json, Schema schema) throws IOException {
	    InputStream input = null;
	    GenericDatumWriter<GenericRecord> writer = null;
	    Encoder encoder = null;
	    ByteArrayOutputStream output = null;
	    
	    try {
	        DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>(schema);
	        input = new ByteArrayInputStream(json.getBytes());
	        output = new ByteArrayOutputStream();
	        DataInputStream din = new DataInputStream(input);
	        writer = new GenericDatumWriter<GenericRecord>(schema);
	        Decoder decoder = DecoderFactory.get().jsonDecoder(schema, din);
	        encoder = EncoderFactory.get().binaryEncoder(output, null);
	        GenericRecord datum;
	        while (true) {
	            try {
	                datum = reader.read(null, decoder);
	            } catch (EOFException eofe) {
	                break;
	            }
	            writer.write(datum, encoder);
	        }
	        encoder.flush();
	        return output.toByteArray();
	    } finally {
	        try { input.close(); } catch (Exception e) { }
	    }
	}
	
	public static String avroToJson(byte[] avro, Schema schema) throws IOException {
	    boolean pretty = false;
	    GenericDatumReader<GenericRecord> reader = null;
	    JsonEncoder encoder = null;
	    ByteArrayOutputStream output = null;
	    try {
	        reader = new GenericDatumReader<GenericRecord>(schema);
	        InputStream input = new ByteArrayInputStream(avro);
	        output = new ByteArrayOutputStream();
	        DatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(schema);
	        encoder = EncoderFactory.get().jsonEncoder(schema, output, pretty);
	        Decoder decoder = DecoderFactory.get().binaryDecoder(input, null);
	        GenericRecord datum;
	        while (true) {
	            try {
	                datum = reader.read(null, decoder);
	            } catch (EOFException eofe) {
	                break;
	            }
	            writer.write(datum, encoder);
	        }
	        encoder.flush();
	        output.flush();
	        return new String(output.toByteArray());
	    } finally {
	        try { if (output != null) output.close(); } catch (Exception e) { }
	    }
	}

}
