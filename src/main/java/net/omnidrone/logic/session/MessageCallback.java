package net.omnidrone.logic.session;

public interface MessageCallback {

	/**
	 * /**
	 * Send message proto as bytes back to client
	 * @param data
	 */
	void send(byte[] data);
}
