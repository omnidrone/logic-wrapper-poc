package net.omnidrone.logic.session;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.logic.session.SessionJNA.CSessionLibrary.DataStruct;
import net.omnidrone.logic.session.SessionJNA.CSessionLibrary.KeyStruct;

public class SessionJNAJson implements Session {

	JsonHelper jsonHelper = new JsonHelper();
	ObjectMapper mapper = new ObjectMapper();
	CSessionLibrary.Functions funcs = new CSessionLibrary.Functions();
	static CSessionLibrary clib;
	static  {
		clib = (CSessionLibrary) Native.loadLibrary("c_wrapper-d", CSessionLibrary.class);		
	}

	String userId;
	Pointer self;
		
	public interface CSessionLibrary extends Library {
		
		public static class IdStruct extends Structure {
			public static class ByValue extends KeyStruct implements Structure.ByValue {
			}
			public static class ByReference extends KeyStruct implements Structure.ByReference {
			}

			public String table;
			public String id;
			public String column;

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("table", "id", "column");
			}
		}
				
		public class Functions extends Structure {

			//Map<String, Object> get(String table, String id);
			public static interface get extends Callback {
				String invoke(String table, String id);
			}			
			//Map<String, Object> getAttributes(String table, String id, List<String> attributes);
			public static interface getAttribute extends Callback {
				String invoke(String table, String id, String attribute);
			}
			
			public static interface getAttributes extends Callback {
				String invoke(String table, String id, Pointer attributes, int noAttributes);
			}
			//Map<String, Object> getMultipleIds(Set<String> ids, String table, List<String> attributes);		
			//Map<String, Map<String, Object>> getForMultipleIdAndMultipleTables(Set<String> ids, Map<String, List<String>> tableAttributes);
			//Map<String, Object> getFromMultipleTables(String id, Map<String, List<String>> tableAttributes);
			//Map<String, Object> getMultipleIdsAll(Set<String> ids, String table);		
			//Map<String, Map<String, Object>> getForMultipleIdAndMultipleTablesAll(Set<String> ids, List<String> tables);
			//Map<String, Object> getFromMultipleTablesAll(String id, List<String> tables);
			//void update(String table, String json);
			//void update(String table, String id, String json);
			
			public static interface update extends Callback {
				void invoke(String table, String id, String json);
			}

			//void updateAttribute(String table, String id, String attribute, Object value);
			public static interface updateAttributes extends Callback {
				void invoke(String table, String id, String json);
			}

			//void updateAttributes(String table, String id, Map<String, Object> attributeValues);
			//void delete(String table, String id);
			
			//void deleteAttributes(String table, String id, String ... columns);
			
			public static interface flushData extends Callback {
				void invoke();
			}

			public static interface sendMessage extends Callback {
				void invoke(Pointer bytes, int length);
			}

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("get", "getAttribute", "getAttributes", "update", "updateAttributes", "flushData", "sendMessage");
			}

			public get get;
			public getAttribute getAttribute;
			public getAttributes getAttributes;
			public update update;
			public updateAttributes updateAttributes;
			public flushData flushData;
			public sendMessage sendMessage;

		}

		public Pointer session_ctor(String userId);
		
		public int session_init(Pointer session, Functions.get get, Functions.getAttribute getAttribute, Functions.getAttributes getAttributes, Functions.update update,
				Functions.updateAttributes updateAttributes, Functions.flushData flushData, Functions.sendMessage sendMessage);

		public void session_onMessage(Pointer session, Pointer bytes, int numBytes);
		
	}

	@Override
	public boolean init(String userId, JsonPersistence jsonPersistence, MessageCallback messageCallback) {
		this.userId = userId;
		funcs.get = new CSessionLibrary.Functions.get() {
			
			@Override
			public String invoke(String table, String id) {
				System.err.println("get invoked from C");
				Map<String, Object> map = jsonPersistence.get(table, id);
				System.err.println("REMOVE Id from map");
				System.err.println(map.remove("Id"));
				try {
					return mapper.writeValueAsString(map);
				} catch (Exception e) {
					e.printStackTrace();
					return returnError(1, e.getMessage());
				}
			}
		};
		funcs.getAttribute = new CSessionLibrary.Functions.getAttribute() {
			
			@Override
			public String invoke(String table, String id, String attribute) {
				System.err.println("getAttributes invoked from C");
				System.err.println("table:" + table);
				System.err.println("id:" + id);
				System.err.println("attribute:" + attribute);
				Map<String, Object> map = jsonPersistence.getAttribute(table, id, attribute);
				
				try {
					return mapper.writeValueAsString(map);
				} catch (Exception e) {
					e.printStackTrace();
					return returnError(1, e.getMessage());
				}
			}
		};

		funcs.getAttributes = new CSessionLibrary.Functions.getAttributes() {
			
			@Override
			public String invoke(String table, String id, Pointer attributesPtr, int noAttributes) {
				System.err.println("getAttributes invoked from C");
				System.err.println("table:" + table);
				System.err.println("id:" + id);
				System.err.println("attributes:" + attributesPtr);
				// getStringArray copies the contents of the C-allocated memory buffer into a Java-managed String[]
				String[] attributes = attributesPtr.getStringArray(0, noAttributes);
				System.err.println("attributes:" + attributes);
				for (String a : attributes) {
					System.err.println(a);
				}
				Map<String, Object> map = jsonPersistence.getAttributes(table, id, Arrays.asList(attributes));
				
				try {
					return mapper.writeValueAsString(map);
				} catch (Exception e) {
					e.printStackTrace();
					return returnError(1, e.getMessage());
				}
			}
		};

		funcs.update = new CSessionLibrary.Functions.update() {
			
			@Override
			public void invoke(String table, String id, String json) {
				System.err.println("update2 invoked from C for " + json);
				//json = json.replace("\U0000", ""); // removes NUL chars
				//json = json.replace("\\U0000", ""); // removes backslash+u0000
				json = json.replace("\\U0000", "\\u0000");
				System.err.println(json);
				//mapper.setDeserializationConfig(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
				//mapper = mapper.setSerializationInclusion(Include.NON_NULL);
				//mapper.configure(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, false);
				//mapper.configure(Feature.WRITE_NULL_PROPERTIES, false);
				//mapper.configure(Feature.WRITE_EMPTY_JSON_ARRAYS, false);
				//mapper.read
				jsonPersistence.update(table, id, json);
			}
		};
		
		funcs.updateAttributes = new CSessionLibrary.Functions.updateAttributes() {
			
			@Override
			public void invoke(String table, String id, String json) {
				System.err.println("updateAttributes invoked from C");
				Map<String, Object> attributeValues;
				try {
					attributeValues = mapper.readValue(json, Map.class);
					jsonPersistence.updateAttributes(table, userId, attributeValues);;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		funcs.flushData = new CSessionLibrary.Functions.flushData() {

			@Override
			public void invoke() {
				System.err.println("Flush data invoked from C");
				//persistenceHandle.flushData();
			}

		};
		
		funcs.sendMessage = new CSessionLibrary.Functions.sendMessage() {

			@Override
			public void invoke(Pointer bytes, int length) {
				System.err.println("Send message invoked from C");
				System.err.println("message callback length:" + length);
				System.err.println("message callback:" + bytes.getByteArray(0, length));
				messageCallback.send(bytes.getByteArray(0, length));
			}
		};

		System.err.println("Init C session");
		// call the C function
		Pointer self = clib.session_ctor(userId);
		clib.session_init(self, funcs.get, funcs.getAttribute, funcs.getAttributes, funcs.update, funcs.updateAttributes, funcs.flushData, funcs.sendMessage);
		this.self = self;
		return true;
	}

	@Override
	public void onMessage(byte[] data) {
		// convert byte array
		// Memory allocates a contiguous block of memory suitable for sharing
		// with C directly - no copying is necessary
		Pointer bytesRef = new Memory(data.length);
		// note: Memory instance (and its associated memory allocation) will be
		// freed when ex8p goes out of scope
		for (int bloop = 0; bloop < data.length; bloop++) {
			// populate the array with junk data (just for the sake of the
			// example)
			bytesRef.setByte(bloop, data[bloop]);
		}
		System.err.println("OnMessage C session");

		clib.session_onMessage(this.self, bytesRef, data.length);
		
		System.err.println("OnMessage C session finished executing");
	}
	
	public String getUserId() {
		return userId;
	}
	
	private String returnError(int code, String message) {
		Map<String, String> json = new HashMap<String, String>();
		json.put("code", ""+code);
		json.put("message", message);
		return jsonHelper.toJson(json);
	}


}
