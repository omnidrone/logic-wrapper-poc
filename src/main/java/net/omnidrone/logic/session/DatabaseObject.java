package net.omnidrone.logic.session;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class DatabaseObject {

	static final ObjectMapper mapper = new ObjectMapper();

	Map<String, Object> value;

	public DatabaseObject(String json) {
		TypeFactory typeFactory = mapper.getTypeFactory();
		MapType mapType = typeFactory.constructMapType(HashMap.class, String.class, Object.class);
		try {
			value = mapper.readValue(json, mapType);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public DatabaseObject(Map<String, Object> value) {
		this.value = value;
	}
	
	public DatabaseObject() {
		value = new HashMap<String, Object>();
	}
	
	

	public Map<String, Object> getValue() {
		return value;
	}
	
	public Map<String, Object> getPrepared(Map<String, Object> map) {

	    Map<String, Object> retVal = new HashMap<String, Object>();

	    for (Map.Entry<String, Object> entry : map.entrySet()) {
	        
	        if (entry.getValue() instanceof DatabaseObject) {
	        	System.err.println("skip:" + entry.getKey());
	            retVal.put(entry.getKey(), getPrepared(((DatabaseObject)entry.getValue()).value));
	            //getValues((Map) entry.getValue());
	        } else {
	        	System.err.println("put:" + entry.getKey());
	            retVal.put(entry.getKey(), entry.getValue());
	        }
	    }

	    return retVal;
	}

	//
    // Summary:
    //     Get the names of all properties on this object
    public Collection<String> properties() {
    	return value.keySet();
    }

    // Summary:
    //     Removes all properties on this object
    public void Clear() {
    	value.clear();
    }
    //
    // Summary:
    //     Returns true if the given property exists on this object
    public boolean Contains(String propertyExpression) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	return node.containsKey(leafKey);
    }
    
    // Summary:
    //     Get the given property as an object
    public Object get(String propertyExpression) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	return node.get(leafKey);
    }
    
    private Map<String, Object> getNode(String propertyExpression) {
    	if (propertyExpression.contains(".")) {
        	String[] path = propertyExpression.split("\\.");
        	if (path.length > 1) {
        		Map<String, Object> node = value;
        		String key = path[0];
        		for (int i=0;i<path.length;i++) {
        			if ((i + 1) < path.length) {
        				node = (Map<String, Object>)node.get(path[i]);
        			}
        			key = path[i];
        		}
				return node;
        	}
    	}
        return value;
    }
    private String leafKey(String propertyExpression) {
    	if (propertyExpression.contains(".")) {
    		String[] path = propertyExpression.split("\\.");
        	return path[path.length -1];
    	}
    	return propertyExpression;
    }
    //
    // Summary:
    //     Get the given array property
    //public DatabaseArray GetArray(String propertyExpression);
    //
    // Summary:
    //     Get the given boolean property
    public boolean Getboolean(String propertyExpression) {
    	return (boolean)get(propertyExpression);
    }
    //
    // Summary:
    //     Get the given boolean property, falling back to the given default value if the
    //     boolean property does not exist
    public boolean Getboolean(String propertyExpression, boolean defaultValue) {
    	if (Contains(propertyExpression))
    		return (boolean)get(propertyExpression);
    	else
    		return defaultValue;

    }
    //
    // Summary:
    //     Get the given byte array property
    public byte[] GetBytes(String propertyExpression){
    	return (byte[])get(propertyExpression);
    }
    //
    // Summary:
    //     Get the given byte array property, falling back to the given default value
    //     if the byte array property does not exist
    public byte[] GetBytes(String propertyExpression, byte[] defaultValue) {
    	if (Contains(propertyExpression))
    		return (byte[])get(propertyExpression);
    	else
    		return defaultValue;
    }
    //
    // Summary:
    //     Get the given datetime property
    public DateTime GetDateTime(String propertyExpression) {
    	long time = (long)get(propertyExpression);
    	return new DateTime(time);
    }
    //
    // Summary:
    //     Get the given datetime property, falling back to the given default value
    //     if the datetime property does not exist
    public DateTime GetDateTime(String propertyExpression, DateTime defaultValue) {
    	if (Contains(propertyExpression)) {
    		long time = (long)get(propertyExpression);
    		return new DateTime(time);
    	}
    	return defaultValue;
    }
    //
    // Summary:
    //     Get the given double property
    public double GetDouble(String propertyExpression) {
		return (double)get(propertyExpression);

    }
    //
    // Summary:
    //     Get the given double property, falling back to the given default value if
    //     the double property does not exist
    public double GetDouble(String propertyExpression, double defaultValue) {
    	if (Contains(propertyExpression))
    		return (double)get(propertyExpression);
    	else
    		return defaultValue;
    }
    
    //
    // Summary:
    //     Get the given float property
    public float GetFloat(String propertyExpression) {
    	return (float)get(propertyExpression);
    }
    //
    // Summary:
    //     Get the given float property, falling back to the given default value if
    //     the float property does not exist
    public float GetFloat(String propertyExpression, float defaultValue){
    	if (Contains(propertyExpression))
    		return (float)get(propertyExpression);
    	else
    		return defaultValue;
    }
    //
    // Summary:
    //     Get the given int property
    public int GetInt(String propertyExpression) {
    	Object number = get(propertyExpression);
    	if (number instanceof BigDecimal) {
        	return ((BigDecimal)number).intValue();
    	}
    	return (int)number;
    }
    //
    // Summary:
    //     Get the given int property, falling back to the given default value if the
    //     int property does not exist
    public int GetInt(String propertyExpression, int defaultValue) {
    	if (Contains(propertyExpression)) {
        	Object number = get(propertyExpression);
        	if (number instanceof BigDecimal) {
            	return ((BigDecimal)number).intValue();
        	}
        	return (int)number;
    	} else
    		return defaultValue;
    }
    //
    // Summary:
    //     Get the given long property
    public long GetLong(String propertyExpression) {
    	return (long)get(propertyExpression);
    }
    //
    // Summary:
    //     Get the given long property, falling back to the given default value if the
    //     long property does not exist
    public long GetLong(String propertyExpression, long defaultValue) {
    	if (Contains(propertyExpression))
    		return (long)get(propertyExpression);
    	else
    		return defaultValue;
    }
    //
    // Summary:
    //     Get the given object property
    public DatabaseObject GetObject(String propertyExpression) {
    	Map<String, Object> map = (Map<String, Object>)get(propertyExpression);
    	return new DatabaseObject(map);
    }
    //
    // Summary:
    //     Get the given String property
    public String GetString(String propertyExpression) {
    	return (String)get(propertyExpression);
    }
    //
    // Summary:
    //     Get the given String property, falling back to the given default value if
    //     the String property does not exist
    public String GetString(String propertyExpression, String defaultValue) {
    	if (Contains(propertyExpression))
    		return (String)get(propertyExpression);
    	else
    		return defaultValue;
    }
    //
    // Summary:
    //     Get the given uint property
    //public uint GetUInt(String propertyExpression);
    //
    // Summary:
    //     Get the given uint property, falling back to the given default value if the
    //     uint property does not exist
    //public uint GetUInt(String propertyExpression, uint defaultValue);
    //
    // Summary:
    //     Get the given property as an object
    public Object GetValue(String propertyExpression) {
    	return get(propertyExpression);
    }
    //
    // Summary:
    //     Removes the value of the given property from this object
    public void Remove(String propertyExpression) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.remove(leafKey);
    }

    //
    // Summary:
    //     Set the given property to the given boolean value
    public void Set(String propertyExpression, boolean value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given byte array value
    public void Set(String propertyExpression, byte[] value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given array
    //public void Set(String propertyExpression, DatabaseArray value);
    //
    // Summary:
    //     Set the given property to the given object
    public void Set(String propertyExpression, DatabaseObject value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given datetime value
    public void Set(String propertyExpression, DateTime value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value.getMillis());
    }
    //
    // Summary:
    //     Set the given property to the given double value
    public void Set(String propertyExpression, double value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given float value
    public void Set(String propertyExpression, float value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given int value
    public void Set(String propertyExpression, int value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given long value
    public void Set(String propertyExpression, long value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given String value
    public void Set(String propertyExpression, String value) {
    	Map<String, Object> node = getNode(propertyExpression);
    	String leafKey = leafKey(propertyExpression);
    	node.put(leafKey, value);
    }
    //
    // Summary:
    //     Set the given property to the given uint value
    //public void Set(String propertyExpression, uint value);
    
    public String ToString() {
    	return value.toString();
    }
}
