package net.omnidrone.logic.session;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.logic.session.SessionJNA.CSessionLibrary.DataStruct;
import net.omnidrone.logic.session.SessionJNA.CSessionLibrary.KeyStruct;

public class SessionJNA implements Session {

	JsonHelper jsonHelper = new JsonHelper();
	CSessionLibrary.Functions funcs = new CSessionLibrary.Functions();
	static CSessionLibrary clib;
	static  {
		clib = (CSessionLibrary) Native.loadLibrary("c_wrapper-d", CSessionLibrary.class);		
	}

	String userId;
	Pointer self;
		
	public interface CSessionLibrary extends Library {

		public static class KeyStruct extends Structure {
			public static class ByValue extends KeyStruct implements Structure.ByValue {
			}
			public static class ByReference extends KeyStruct implements Structure.ByReference {
			}

			public String table;
			public String id;
			public String column;

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("table", "id", "column");
			}
		}

		public static class DataStruct extends Structure {
			public static class ByValue extends DataStruct implements Structure.ByValue {
			}

			public KeyStruct key;
			public String data;

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("key", "data");
			}
		}

		public class Functions extends Structure {

			public static interface loadData extends Callback {
				String invoke(KeyStruct.ByValue key);
			}

			public static interface updateData extends Callback {
				void invoke(DataStruct.ByValue dataStruct);
			}

			public static interface flushData extends Callback {
				void invoke();
			}

			public static interface sendMessage extends Callback {
				void invoke(Pointer bytes, int length);
			}

			@Override
			protected List getFieldOrder() {
				return Arrays.asList("loadData", "updateData", "flushData", "sendMessage");
			}

			public loadData loadData;
			public updateData updateData;
			public flushData flushData;
			public sendMessage sendMessage;

		}

		public Pointer session_ctor(String userId);
		
		public int session_init(Pointer session, Functions.loadData loadData, Functions.updateData updateData,
				Functions.flushData flushData, Functions.sendMessage sendMessage);

		public void session_onMessage(Pointer session, Pointer bytes, int numBytes);
		
	}

	@Override
	public boolean init(String userId, JsonPersistence jsonPersistence, MessageCallback messageCallback) {
		this.userId = userId;
		funcs.loadData = new CSessionLibrary.Functions.loadData() {

			@Override
			public String invoke(KeyStruct.ByValue keyStruct) {
				System.err.println("Load data invoked from C");				
				
				System.err.println(
						"Load data invoked for " + keyStruct.table + ", " + keyStruct.id + ", " + keyStruct.column);
				Key key = new Key(keyStruct.table, keyStruct.id, keyStruct.column);
				Map<String, Object> map = jsonPersistence.getAttributes(keyStruct.table, keyStruct.id, Arrays.asList(keyStruct.column));
				//Map<String, String> map = persistenceHandle.loadRequiredData(key);
				
				String[] datas = new String[map.size()];
				int i = 0;
				System.err.println("UNCOMMENT OR IT WILL BREAK");
//				for (Map.Entry<String, Object> entry : map.entrySet()) {
//					String entryJson = jsonHelper.jsonFrom(key, entry.getValue());
//					datas[i] = entryJson;
//					i++;
//				}
				String data = jsonHelper.jsonFrom(datas);
				System.err.println("Pass to C: " + data);
				return data;
			}
		};

		funcs.updateData = new CSessionLibrary.Functions.updateData() {

			@Override
			public void invoke(DataStruct.ByValue dataStruct) {
				System.err.println("Update data invoked from C");
				Map<Key, String> data = new HashMap<Key, String>();
				Key key = new Key(dataStruct.key.table, dataStruct.key.id, dataStruct.key.column);
				data.put(key, dataStruct.data);
				jsonPersistence.updateAttribute(dataStruct.key.table, dataStruct.key.id, dataStruct.key.column, dataStruct.data);
				//persistenceHandle.updateData(data);
			}

		};

		funcs.flushData = new CSessionLibrary.Functions.flushData() {

			@Override
			public void invoke() {
				System.err.println("Flush data invoked from C");
				//persistenceHandle.flushData();
			}

		};

		funcs.sendMessage = new CSessionLibrary.Functions.sendMessage() {

			@Override
			public void invoke(Pointer bytes, int length) {
				System.err.println("Send message invoked from C");
				System.err.println("message callback length:" + length);
				System.err.println("message callback:" + bytes.getByteArray(0, length));
				messageCallback.send(bytes.getByteArray(0, length));
			}
		};

		System.err.println("Init C session");
		// call the C function
		Pointer self = clib.session_ctor(userId);
		clib.session_init(self, funcs.loadData, funcs.updateData, funcs.flushData, funcs.sendMessage);
		this.self = self;
		return true;
	}

	@Override
	public void onMessage(byte[] data) {
		// convert byte array
		// Memory allocates a contiguous block of memory suitable for sharing
		// with C directly - no copying is necessary
		Pointer bytesRef = new Memory(data.length);
		// note: Memory instance (and its associated memory allocation) will be
		// freed when ex8p goes out of scope
		for (int bloop = 0; bloop < data.length; bloop++) {
			// populate the array with junk data (just for the sake of the
			// example)
			bytesRef.setByte(bloop, data[bloop]);
		}
		System.err.println("OnMessage C session");

		clib.session_onMessage(this.self, bytesRef, data.length);
		
		System.err.println("OnMessage C session finished executing");
	}
	
	public String getUserId() {
		return userId;
	}


}
