package net.omnidrone.logic.session.rpc;

import io.netty.channel.ChannelHandlerContext;
import net.omnidrone.game.protobuf.GameProtos.GameCommand;
import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.logic.session.MessageCallback;

public interface SessionRPC {
	
	/**
	 * Initialize the command with the id of the user and callbacks to persistence and client
	 * @param userId
	 * @param persistenceHandle
	 * @return false if not initialized correctly
	 */
	boolean init(String userId);
	
	
	/**
	 * Incoming message proto passed as bytes
	 * @return
	 */
	void onMessage(String userId, ChannelHandlerContext ctx, GameCommand gameCommand);
}
