package net.omnidrone.logic.session.rpc;

import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Singleton;
import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.FormatFactory;
import com.googlecode.protobuf.format.JsonFormat;
import com.googlecode.protobuf.format.ProtobufFormatter;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.netty.channel.ChannelHandlerContext;
import net.omnidrone.game.protobuf.GameProtos;
import net.omnidrone.game.protobuf.GameProtos.GameCommand;
import net.omnidrone.game.protobuf.GameProtos.GetSpaceship;
import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.logic.session.MessageCallback;
import net.omnidrone.logic.session.Session;
import net.omnidrone.service.CppRPCGrpc;
import net.omnidrone.service.CppRPCGrpc.CppRPC;
import net.omnidrone.service.ServiceCpp.GameRequest;
import net.omnidrone.service.ServiceCpp.GameResponse;
import net.omnidrone.service.ServiceCpp.InitRequest;
import net.omnidrone.service.ServiceCpp.InitResponse;

@Singleton
public class SessionRPCImpl implements SessionRPC {
	private static final Logger logger = LoggerFactory.getLogger(SessionRPCImpl.class);

	private static final FormatFactory FORMAT_FACTORY = new FormatFactory();
	private static final ProtobufFormatter JSON_FORMATTER = FORMAT_FACTORY
			.createFormatter(FormatFactory.Formatter.JSON);

	private final ManagedChannel channel;
	private final CppRPCGrpc.CppRPCBlockingStub blockingStub;

	/**
	 * Construct client connecting to HelloWorld server at {@code host:port}.
	 */
	public SessionRPCImpl(String host, int port) {
		channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();
		blockingStub = CppRPCGrpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	@Override
	public boolean init(String userId) {
		try {
			logger.info("session init");
			InitRequest request = InitRequest.newBuilder().setUserId(userId).build();
			InitResponse response = blockingStub.init(request);
			logger.info("init response: " + response.getSuccess());
			return response.getSuccess();
		} catch (RuntimeException e) {
			logger.warn("RPC failed", e);
			return false;
		}
	}

	@Override
	public void onMessage(String userId, ChannelHandlerContext ctx, GameCommand gameCommand) {
		try {
			logger.info("session onMessage");
			Any any = gameCommand.getCommand();
			System.err.println("is spaceship:" + any.is(GetSpaceship.class));
			
	        try {
				any.unpack(GameProtos.GetSpaceshipResponse.class);
			} catch (InvalidProtocolBufferException e) {
				e.printStackTrace();
			}
			GameRequest request = GameRequest.newBuilder().setUserId(userId).setGameCommand(gameCommand).build();
			GameResponse response = blockingStub.onMessage(request);
			logger.info("onMessage response: " + response);
			System.err.println(JSON_FORMATTER.printToString(response));
			ctx.writeAndFlush(response.getGameCommandResponse());
			// return response.getSuccess();
		} catch (RuntimeException e) {
			logger.warn("RPC failed", e);
			// return false;
		}
	}

}
