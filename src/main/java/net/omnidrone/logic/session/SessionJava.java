package net.omnidrone.logic.session;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;

import net.omnidrone.logic.domain.Spaceship;
import net.omnidrone.logic.persistence.DynamoDBJsonPersistence;
import net.omnidrone.logic.persistence.JsonPersistence;
import net.omnidrone.game.protobuf.GameProtos;

public class SessionJava implements Session {

	private static final String SPACESHIP_TABLE = "spaceship";
	private static final String SPACESHIP_ATTRIBUTE = "spaceship";
	
	String userId;
	//PersistenceHandle persistenceHandle;
	JsonPersistence jsonPersistence;
	MessageCallback messageCallback;

	JsonHelper jsonHelper = new JsonHelper();
	ExtensionRegistry registry;
	
	boolean storeSpaceshipsInSeparateColumns = true;
	
	public SessionJava(ExtensionRegistry registry) {
		this.registry = registry;
	}
	
	@Override
	public boolean init(String userId, JsonPersistence jsonPersistence, MessageCallback messageCallback) {
		this.userId = userId;
		this.jsonPersistence = jsonPersistence;
		//this.persistenceHandle = persistenceHandle;
		this.messageCallback = messageCallback;
		return false;
	}

	@Override
	public void onMessage(byte[] data) {
		/*
		try {
			GameProtos.GameCommand gameCommand = GameProtos.GameCommand.parseFrom(data, registry);
			if (gameCommand.hasExtension(GameProtos.updateVelocity)){
				System.err.println("UPDATE VELOCITY RECEIVED");
				updateVelocity(gameCommand.getExtension(GameProtos.updateVelocity));
			} else if (gameCommand.hasExtension(GameProtos.updateCargo)){				
				System.err.println("UPDATE CARGO RECEIVED");
				updateCargo(gameCommand.getExtension(GameProtos.updateCargo));
			} else if (gameCommand.hasExtension(GameProtos.createSpaceship)){				
				System.err.println("CREATE SPACESHIP RECEIVED");
				createSpaceship(gameCommand.getExtension(GameProtos.createSpaceship));
			} else if (gameCommand.hasExtension(GameProtos.getSpaceship)){
				System.err.println("GET SPACESHIP RECEIVED");
				GameProtos.GetSpaceshipResponse spaceship = getSpaceship(gameCommand.getExtension(GameProtos.getSpaceship));
				GameProtos.GameCommandResponse.Builder builder = GameProtos.GameCommandResponse.newBuilder();
				//builder.setCommandType(GameProtos.GETSPACESHIPRESPONSE_FIELD_NUMBER);	
				builder.setExtension(GameProtos.getSpaceshipResponse, spaceship);
				messageCallback.send(builder.build().toByteArray());
			} else if (gameCommand.hasExtension(GameProtos.getSpaceships)){
				System.err.println("GET SPACESHIPS RECEIVED");
				GameProtos.GetSpaceshipsResponse spaceships = getSpaceships();
				GameProtos.GameCommandResponse.Builder builder = GameProtos.GameCommandResponse.newBuilder();
				//builder.setCommandType(GameProtos.GETSPACESHIPRESPONSE_FIELD_NUMBER);	
				builder.setExtension(GameProtos.getSpaceshipsResponse, spaceships);
				messageCallback.send(builder.build().toByteArray());
			} else {
				System.err.println("Unknown gamecommand: " + gameCommand);
			
			}
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
		}
		*/
	}
	
	public String getUserId() {
		return userId;
	}
	/*
	private void createSpaceship(GameProtos.CreateSpaceship createSpaceship) {
		System.err.println("create spaceship received");

		Spaceship spaceship = new Spaceship();
		spaceship.id = UUID.randomUUID().toString().substring(0, 4);
		spaceship.name = createSpaceship.hasName() ? createSpaceship.getName() : null;
		spaceship.velocity = createSpaceship.getVelocity();
		spaceship.cargo = createSpaceship.hasCargo() ? createSpaceship.getCargo() : null;
		spaceship.crewMembers = createSpaceship.getCrewMembers();
		
		if (storeSpaceshipsInSeparateColumns) {
			String column = SPACESHIP_ATTRIBUTE + spaceship.id;
			
			//try to mimic getting json from c, convert it to map with valid types for dynamo db to store it as a nested object
			String spaceshipJson = jsonHelper.toJson(spaceship);
			Map spaceshipMap = jsonHelper.fromJson(spaceshipJson, Map.class);
			jsonPersistence.updateAttribute(SPACESHIP_TABLE, userId, column, spaceshipMap);			
		} else {
			
			String column = SPACESHIP_ATTRIBUTE;
			Map<String, Object> dynamoSpaceships = jsonPersistence.getAttributes(SPACESHIP_TABLE, userId, Arrays.asList(column));
			for (Map.Entry<String, Object> current : dynamoSpaceships.entrySet()) {
				System.err.println("current: " + current.getKey() + " - " + current.getValue());
			}
			Map<String, Object> currentSpaceships = new HashMap<String, Object>(dynamoSpaceships);
			Map<String, Object> spaceships;
			if (currentSpaceships.containsKey(SPACESHIP_ATTRIBUTE)) {
				spaceships = (Map<String, Object>)currentSpaceships.get(SPACESHIP_ATTRIBUTE);
			} else {
				spaceships = new HashMap<String, Object>();
			}
				
			String spaceshipJson = jsonHelper.toJson(spaceship);
			Map spaceshipMap = jsonHelper.fromJson(spaceshipJson, Map.class);
			spaceships.put(spaceship.id, spaceshipMap);
			//try to mimic getting json from c, convert it to map with valid types for dynamo db to store it as a nested object
			System.err.println("create new map: " + jsonHelper.toJson(spaceships));
			jsonPersistence.updateAttribute(SPACESHIP_TABLE, userId, column, spaceships);				
		}

	}
	private void updateVelocity(GameProtos.UpdateVelocity updateVelocity) {
		System.err.println("update velocity received");
		String column = (!storeSpaceshipsInSeparateColumns) ? SPACESHIP_ATTRIBUTE : SPACESHIP_ATTRIBUTE + updateVelocity.getId();
		String updatePath = (!storeSpaceshipsInSeparateColumns) ? column+"."+updateVelocity.getId()+".velocity" : column + ".velocity";
		jsonPersistence.updateAttribute(SPACESHIP_TABLE, userId, updatePath, updateVelocity.getVelocity());
	}

	private void updateCargo(GameProtos.UpdateCargo updateCargo) {
		System.err.println("update cargo received");
		String column = (!storeSpaceshipsInSeparateColumns) ? SPACESHIP_ATTRIBUTE : SPACESHIP_ATTRIBUTE + updateCargo.getId();
		String updatePath = (!storeSpaceshipsInSeparateColumns) ? column+"."+updateCargo.getId()+".cargo" : column + ".cargo";
		jsonPersistence.updateAttribute(SPACESHIP_TABLE, userId, updatePath, updateCargo.getCargo());
	}

	private GameProtos.GetSpaceshipResponse getSpaceship(GameProtos.GetSpaceship getSpaceship) {
		String column = (!storeSpaceshipsInSeparateColumns) ? SPACESHIP_ATTRIBUTE : SPACESHIP_ATTRIBUTE + getSpaceship.getId();
		Map<String, Object> map = jsonPersistence.getAttributes(SPACESHIP_TABLE, userId, Arrays.asList(column));
		GameProtos.GetSpaceshipResponse.Builder builder = GameProtos.GetSpaceshipResponse.newBuilder();
		if (map.containsKey(column) && map.get(column) != null) {
			Map<String, Object> spaceships = (Map<String, Object>)map.get(column);
			if (!storeSpaceshipsInSeparateColumns) {
				GameProtos.Spaceship spaceship = getSpacehipProtoFromMap((Map<String, Object>)spaceships.get(getSpaceship.getId()));				
				builder.setSpaceship(spaceship);
			} else {
				GameProtos.Spaceship spaceship = getSpacehipProtoFromMap(spaceships);								
				builder.setSpaceship(spaceship);
			}
		} else {
			System.err.println("no spacehsip found");
		}
		return builder.build();
	}

	private GameProtos.GetSpaceshipsResponse getSpaceships() {
		Map<String, Object> map = jsonPersistence.get(SPACESHIP_TABLE, userId);
		GameProtos.GetSpaceshipsResponse.Builder builder = GameProtos.GetSpaceshipsResponse.newBuilder();
		
		if (map != null && !map.isEmpty()) {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().startsWith(SPACESHIP_ATTRIBUTE) && entry.getValue() != null) {
					if (!storeSpaceshipsInSeparateColumns) {
						Map<String, Object> spaceships = (Map<String, Object>)entry.getValue();
						for (Object s : spaceships.values()) {
							GameProtos.Spaceship spaceship = getSpacehipProtoFromMap((Map<String, Object>)s);
							builder.addSpaceships(spaceship);
						}						
					} else {
						Map<String, Object> spaceshipMap = (Map<String, Object>)entry.getValue();
						GameProtos.Spaceship spaceship = getSpacehipProtoFromMap(spaceshipMap);
						builder.addSpaceships(spaceship);						
					}
				}
			}
		} else {
			System.err.println("no spacehsip found");
		}
		return builder.build();
	}

//	private GameProtos.Spaceship getSpacehipProtoFromJson(String id, String json) {
//		System.err.println("json from dynamo:" + json);
//		json = json.replace("\\", "");
//		System.err.println("pass to gson:" + json);
//		Spaceship spaceship = jsonHelper.fromJson(json, Spaceship.class);
//		spaceship.id = id;
//		GameProtos.Spaceship.Builder builder = GameProtos.Spaceship.newBuilder();
//		builder.setId(spaceship.id).setVelocity(spaceship.velocity);
//		if (spaceship.name != null)
//			builder.setName(spaceship.name);
//		if (spaceship.cargo != null)
//			builder.setCargo(spaceship.cargo);
//			
//		return builder.build();
//	}

	private GameProtos.Spaceship getSpacehipProtoFromJson(String json) {
		System.err.println("json from dynamo:" + json);
		json = json.replace("\\", "");
		System.err.println("pass to gson:" + json);
		Spaceship spaceship = jsonHelper.fromJson(json, Spaceship.class);
//		spaceship.id = id;
		GameProtos.Spaceship.Builder builder = GameProtos.Spaceship.newBuilder();
		builder.setId(spaceship.id).setVelocity(spaceship.velocity);
		if (spaceship.name != null)
			builder.setName(spaceship.name);
		if (spaceship.cargo != null)
			builder.setCargo(spaceship.cargo);
			
		return builder.build();
	}

	private GameProtos.Spaceship getSpacehipProtoFromMap(Map<String, Object> map) {
		String json = jsonHelper.toJson(map);
		System.err.println("json from dynamo Map:" + json);
		json = json.replace("\\", "");
		System.err.println("pass to gson:" + json);
		Spaceship spaceship = jsonHelper.fromJson(json, Spaceship.class);
//		spaceship.id = id;
		GameProtos.Spaceship.Builder builder = GameProtos.Spaceship.newBuilder();
		builder.setId(spaceship.id).setVelocity(spaceship.velocity);
		if (spaceship.name != null)
			builder.setName(spaceship.name);
		if (spaceship.cargo != null)
			builder.setCargo(spaceship.cargo);
			
		return builder.build();
	}
*/
}