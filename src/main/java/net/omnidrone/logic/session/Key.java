package net.omnidrone.logic.session;

import java.util.Arrays;

public class Key {
	public String table;
	public String id;
	public String[] columns;
	
	public Key(String table, String id, String ... columns) {
		this.table = table;
		this.id = id;
		this.columns = columns;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Key other = (Key)obj;
		if (other.table == null || other.id == null || other.columns == null)
			return false;
		if (other.table.equalsIgnoreCase(table) && other.id.equalsIgnoreCase(id)) {
			//compare columns
			if (this.columns != null && this.columns.length == other.columns.length) {
				return Arrays.equals(this.columns, other.columns);
			}
		}
		return false;
	}
}
