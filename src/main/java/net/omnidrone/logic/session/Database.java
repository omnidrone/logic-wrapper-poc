package net.omnidrone.logic.session;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.omnidrone.logic.persistence.JsonPersistence;

public class Database {

	private JsonPersistence persistence;
	private static final ObjectMapper mapper = new ObjectMapper();

	public Database(JsonPersistence persistence) {
		this.persistence = persistence;
	}
	
	public DatabaseObject loadDatabaseObject(String table, String key) {
		Map<String, Object> map = persistence.get(table, key);
		return new DatabaseObject(map);
	}
	
//	public DatabaseArray loadDatabaseArray(String table, String key) {
//		
//	}
	
	public void saveDatabaseObject(String table, String key, DatabaseObject databaseObject) {
		try {
			
			String json = mapper.writeValueAsString(databaseObject.getPrepared(databaseObject.getValue()));
			System.err.println("save:");
			System.err.println(json);
			persistence.update(table, key, json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
//	public void saveDatabaseArray(String table, String key, DatabaseArray databaseArray) {
//		
//	}
}
