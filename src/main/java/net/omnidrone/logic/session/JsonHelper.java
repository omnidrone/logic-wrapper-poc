package net.omnidrone.logic.session;

import com.google.gson.Gson;

public class JsonHelper {
	Gson gson = new Gson();

	public String jsonFrom(Key key, String data) {
		return gson.toJson(new KeyAndData(key, data)).replace("\\", "");
	}

	public String jsonFrom(String column, String data) {
		return gson.toJson(new ColumnAndData(column, data)).replace("\\", "");
	}

	public String jsonFrom(String[] array) {
		return gson.toJson(array);
	}
	
	public <T> T fromJson(String json, Class<T> clazz) {
		json = json.replace("\\", "");
		System.err.println(json);
		return gson.fromJson(json, clazz);
	}

	public <T> String toJson(T t) {
		return gson.toJson(t);
	}

	private class KeyAndData {
		final String table;
		final String id;
		final String column;
		final String data;

		public KeyAndData(Key key, String data) {
			this.table = key.table;
			this.id = key.id;
			this.column = key.columns[0];
			this.data = data.replaceAll("[\\n\\t ]", "");
		}

	}
	
	private class ColumnAndData {
		final String column;
		final String data;

		public ColumnAndData(String column, String data) {
			this.column = column;
			this.data = data.replaceAll("[\\n\\t ]", "");
		}

	}

}
