package net.omnidrone.logic.session;

import net.omnidrone.logic.persistence.JsonPersistence;

public interface Session {
	/**
	 * Initialize the command with the id of the user and callbacks to persistence and client
	 * @param userId
	 * @param persistenceHandle
	 * @return false if not initialized correctly
	 */
	boolean init(String userId, JsonPersistence jsonPersistence, MessageCallback messageCallback);
	
	
	/**
	 * Incoming message proto passed as bytes
	 * @return
	 */
	void onMessage(byte[] data);
	
	/**
	 * Get the userId of this session
	 * @return
	 */
	String getUserId();

}
