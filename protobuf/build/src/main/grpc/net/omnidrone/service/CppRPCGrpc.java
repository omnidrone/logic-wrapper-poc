package net.omnidrone.service;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;

@javax.annotation.Generated("by gRPC proto compiler")
public class CppRPCGrpc {

  private CppRPCGrpc() {}

  public static final String SERVICE_NAME = "service_cpp.CppRPC";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi
  public static final io.grpc.MethodDescriptor<net.omnidrone.service.ServiceCpp.InitRequest,
      net.omnidrone.service.ServiceCpp.InitResponse> METHOD_INIT =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "service_cpp.CppRPC", "Init"),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceCpp.InitRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceCpp.InitResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi
  public static final io.grpc.MethodDescriptor<net.omnidrone.service.ServiceCpp.GameRequest,
      net.omnidrone.service.ServiceCpp.GameResponse> METHOD_ON_MESSAGE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "service_cpp.CppRPC", "OnMessage"),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceCpp.GameRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceCpp.GameResponse.getDefaultInstance()));

  public static CppRPCStub newStub(io.grpc.Channel channel) {
    return new CppRPCStub(channel);
  }

  public static CppRPCBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CppRPCBlockingStub(channel);
  }

  public static CppRPCFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CppRPCFutureStub(channel);
  }

  public static interface CppRPC {

    public void init(net.omnidrone.service.ServiceCpp.InitRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceCpp.InitResponse> responseObserver);

    public void onMessage(net.omnidrone.service.ServiceCpp.GameRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceCpp.GameResponse> responseObserver);
  }

  public static interface CppRPCBlockingClient {

    public net.omnidrone.service.ServiceCpp.InitResponse init(net.omnidrone.service.ServiceCpp.InitRequest request);

    public net.omnidrone.service.ServiceCpp.GameResponse onMessage(net.omnidrone.service.ServiceCpp.GameRequest request);
  }

  public static interface CppRPCFutureClient {

    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceCpp.InitResponse> init(
        net.omnidrone.service.ServiceCpp.InitRequest request);

    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceCpp.GameResponse> onMessage(
        net.omnidrone.service.ServiceCpp.GameRequest request);
  }

  public static class CppRPCStub extends io.grpc.stub.AbstractStub<CppRPCStub>
      implements CppRPC {
    private CppRPCStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CppRPCStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CppRPCStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CppRPCStub(channel, callOptions);
    }

    @java.lang.Override
    public void init(net.omnidrone.service.ServiceCpp.InitRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceCpp.InitResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_INIT, getCallOptions()), request, responseObserver);
    }

    @java.lang.Override
    public void onMessage(net.omnidrone.service.ServiceCpp.GameRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceCpp.GameResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_ON_MESSAGE, getCallOptions()), request, responseObserver);
    }
  }

  public static class CppRPCBlockingStub extends io.grpc.stub.AbstractStub<CppRPCBlockingStub>
      implements CppRPCBlockingClient {
    private CppRPCBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CppRPCBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CppRPCBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CppRPCBlockingStub(channel, callOptions);
    }

    @java.lang.Override
    public net.omnidrone.service.ServiceCpp.InitResponse init(net.omnidrone.service.ServiceCpp.InitRequest request) {
      return blockingUnaryCall(
          getChannel().newCall(METHOD_INIT, getCallOptions()), request);
    }

    @java.lang.Override
    public net.omnidrone.service.ServiceCpp.GameResponse onMessage(net.omnidrone.service.ServiceCpp.GameRequest request) {
      return blockingUnaryCall(
          getChannel().newCall(METHOD_ON_MESSAGE, getCallOptions()), request);
    }
  }

  public static class CppRPCFutureStub extends io.grpc.stub.AbstractStub<CppRPCFutureStub>
      implements CppRPCFutureClient {
    private CppRPCFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CppRPCFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CppRPCFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CppRPCFutureStub(channel, callOptions);
    }

    @java.lang.Override
    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceCpp.InitResponse> init(
        net.omnidrone.service.ServiceCpp.InitRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_INIT, getCallOptions()), request);
    }

    @java.lang.Override
    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceCpp.GameResponse> onMessage(
        net.omnidrone.service.ServiceCpp.GameRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_ON_MESSAGE, getCallOptions()), request);
    }
  }

  public static io.grpc.ServerServiceDefinition bindService(
      final CppRPC serviceImpl) {
    return io.grpc.ServerServiceDefinition.builder(SERVICE_NAME)
      .addMethod(
        METHOD_INIT,
        asyncUnaryCall(
          new io.grpc.stub.ServerCalls.UnaryMethod<
              net.omnidrone.service.ServiceCpp.InitRequest,
              net.omnidrone.service.ServiceCpp.InitResponse>() {
            @java.lang.Override
            public void invoke(
                net.omnidrone.service.ServiceCpp.InitRequest request,
                io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceCpp.InitResponse> responseObserver) {
              serviceImpl.init(request, responseObserver);
            }
          }))
      .addMethod(
        METHOD_ON_MESSAGE,
        asyncUnaryCall(
          new io.grpc.stub.ServerCalls.UnaryMethod<
              net.omnidrone.service.ServiceCpp.GameRequest,
              net.omnidrone.service.ServiceCpp.GameResponse>() {
            @java.lang.Override
            public void invoke(
                net.omnidrone.service.ServiceCpp.GameRequest request,
                io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceCpp.GameResponse> responseObserver) {
              serviceImpl.onMessage(request, responseObserver);
            }
          })).build();
  }
}
