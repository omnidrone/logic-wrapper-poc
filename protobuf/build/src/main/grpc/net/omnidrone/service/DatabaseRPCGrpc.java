package net.omnidrone.service;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;

@javax.annotation.Generated("by gRPC proto compiler")
public class DatabaseRPCGrpc {

  private DatabaseRPCGrpc() {}

  public static final String SERVICE_NAME = "service_db.DatabaseRPC";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi
  public static final io.grpc.MethodDescriptor<net.omnidrone.service.ServiceDb.GetRequest,
      net.omnidrone.service.ServiceDb.GetResponse> METHOD_GET =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "service_db.DatabaseRPC", "Get"),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceDb.GetRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceDb.GetResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi
  public static final io.grpc.MethodDescriptor<net.omnidrone.service.ServiceDb.UpdateRequest,
      net.omnidrone.service.ServiceDb.UpdateResponse> METHOD_UPDATE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "service_db.DatabaseRPC", "Update"),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceDb.UpdateRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(net.omnidrone.service.ServiceDb.UpdateResponse.getDefaultInstance()));

  public static DatabaseRPCStub newStub(io.grpc.Channel channel) {
    return new DatabaseRPCStub(channel);
  }

  public static DatabaseRPCBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DatabaseRPCBlockingStub(channel);
  }

  public static DatabaseRPCFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DatabaseRPCFutureStub(channel);
  }

  public static interface DatabaseRPC {

    public void get(net.omnidrone.service.ServiceDb.GetRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceDb.GetResponse> responseObserver);

    public void update(net.omnidrone.service.ServiceDb.UpdateRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceDb.UpdateResponse> responseObserver);
  }

  public static interface DatabaseRPCBlockingClient {

    public net.omnidrone.service.ServiceDb.GetResponse get(net.omnidrone.service.ServiceDb.GetRequest request);

    public net.omnidrone.service.ServiceDb.UpdateResponse update(net.omnidrone.service.ServiceDb.UpdateRequest request);
  }

  public static interface DatabaseRPCFutureClient {

    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceDb.GetResponse> get(
        net.omnidrone.service.ServiceDb.GetRequest request);

    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceDb.UpdateResponse> update(
        net.omnidrone.service.ServiceDb.UpdateRequest request);
  }

  public static class DatabaseRPCStub extends io.grpc.stub.AbstractStub<DatabaseRPCStub>
      implements DatabaseRPC {
    private DatabaseRPCStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DatabaseRPCStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DatabaseRPCStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DatabaseRPCStub(channel, callOptions);
    }

    @java.lang.Override
    public void get(net.omnidrone.service.ServiceDb.GetRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceDb.GetResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET, getCallOptions()), request, responseObserver);
    }

    @java.lang.Override
    public void update(net.omnidrone.service.ServiceDb.UpdateRequest request,
        io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceDb.UpdateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_UPDATE, getCallOptions()), request, responseObserver);
    }
  }

  public static class DatabaseRPCBlockingStub extends io.grpc.stub.AbstractStub<DatabaseRPCBlockingStub>
      implements DatabaseRPCBlockingClient {
    private DatabaseRPCBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DatabaseRPCBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DatabaseRPCBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DatabaseRPCBlockingStub(channel, callOptions);
    }

    @java.lang.Override
    public net.omnidrone.service.ServiceDb.GetResponse get(net.omnidrone.service.ServiceDb.GetRequest request) {
      return blockingUnaryCall(
          getChannel().newCall(METHOD_GET, getCallOptions()), request);
    }

    @java.lang.Override
    public net.omnidrone.service.ServiceDb.UpdateResponse update(net.omnidrone.service.ServiceDb.UpdateRequest request) {
      return blockingUnaryCall(
          getChannel().newCall(METHOD_UPDATE, getCallOptions()), request);
    }
  }

  public static class DatabaseRPCFutureStub extends io.grpc.stub.AbstractStub<DatabaseRPCFutureStub>
      implements DatabaseRPCFutureClient {
    private DatabaseRPCFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DatabaseRPCFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DatabaseRPCFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DatabaseRPCFutureStub(channel, callOptions);
    }

    @java.lang.Override
    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceDb.GetResponse> get(
        net.omnidrone.service.ServiceDb.GetRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET, getCallOptions()), request);
    }

    @java.lang.Override
    public com.google.common.util.concurrent.ListenableFuture<net.omnidrone.service.ServiceDb.UpdateResponse> update(
        net.omnidrone.service.ServiceDb.UpdateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_UPDATE, getCallOptions()), request);
    }
  }

  public static io.grpc.ServerServiceDefinition bindService(
      final DatabaseRPC serviceImpl) {
    return io.grpc.ServerServiceDefinition.builder(SERVICE_NAME)
      .addMethod(
        METHOD_GET,
        asyncUnaryCall(
          new io.grpc.stub.ServerCalls.UnaryMethod<
              net.omnidrone.service.ServiceDb.GetRequest,
              net.omnidrone.service.ServiceDb.GetResponse>() {
            @java.lang.Override
            public void invoke(
                net.omnidrone.service.ServiceDb.GetRequest request,
                io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceDb.GetResponse> responseObserver) {
              serviceImpl.get(request, responseObserver);
            }
          }))
      .addMethod(
        METHOD_UPDATE,
        asyncUnaryCall(
          new io.grpc.stub.ServerCalls.UnaryMethod<
              net.omnidrone.service.ServiceDb.UpdateRequest,
              net.omnidrone.service.ServiceDb.UpdateResponse>() {
            @java.lang.Override
            public void invoke(
                net.omnidrone.service.ServiceDb.UpdateRequest request,
                io.grpc.stub.StreamObserver<net.omnidrone.service.ServiceDb.UpdateResponse> responseObserver) {
              serviceImpl.update(request, responseObserver);
            }
          })).build();
  }
}
