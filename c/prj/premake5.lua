-------------------------------------------------------------------------------

include "../tools/premake/common/"

-------------------------------------------------------------------------------

solution "test"
	math.randomseed(string.hash(solution().name))
	startproject "test"
	addCommonConfig()

-------------------------------------------------------------------------------

	configuration "Debug"
		targetsuffix "-d"

        configuration { "macosx", "gmake" }
			buildoptions { "-mmacosx-version-min=10.7" }
			linkoptions  { "-mmacosx-version-min=10.7" }

-------------------------------------------------------------------------------

	local LIBS = "../libs/"
	local SRC  = "../src/"

	local BUILD_DIR   = "../build/".._OPTIONS["arch"].."/"
	local RELEASE_DIR = "../release/".._OPTIONS["arch"].."/"

	--local PROTOBUFFERS = "../libs/protobuf/"
	local PROTOBUFFERS = "../protobuf/src"
	local SESSION      = "../libs/sessionlib/"
	local C_WRAPPER    = "../libs/c_wrapper/"

-------------------------------------------------------------------------------

project "session"
	targetdir(BUILD_DIR)
	kind "StaticLib"
	language "C++"

	includedirs
	{
		SESSION .. "include/",
		PROTOBUFFERS .. "",
	}

	files
	{
		SESSION .. "**",
		SESSION .. "include/*.cpp",
	}

	targetname("session" .. _OPTIONS["arch"])
	
	configuration "Debug"
		defines { "_DEBUG" }

	configuration "Release"
		defines { "NDEBUG" }
	
        configuration { "macosx", "gmake" }
		toolset "clang"

	configuration "gmake"
		buildoptions {"-std=gnu++11 -fPIC"}	

        configuration "macosx"
                linkoptions  {"-std=c++11", "-stdlib=libc++","-fPIC"}
		buildoptions {"-std=c++11", "-stdlib=libc++","-fPIC"}	

-------------------------------------------------------------------------------

project "c_wrapper"
	targetdir(BUILD_DIR)
	kind "SharedLib"
	language "C++"

	includedirs
	{
		SESSION .. "include",
	}

	files
	{
		C_WRAPPER .. "**",
	}
	
	libdirs
	{
		BUILD_DIR,
		"/usr/local/lib/",
	}
	
	links
	{
		"session"
	}
    
        configuration { "macosx", "gmake" }
		toolset "clang"


	configuration "Debug"
		libdirs 
		{ 
			PROTOBUFFERS .. "debug/",
		}

	configuration "Release"
		libdirs
		{
			PROTOBUFFERS .. "release/",
		}

	targetname("c_wrapper" .. _OPTIONS["arch"])
	
	configuration "Debug"
		defines { "_DEBUG" }

	configuration "Release"
		defines { "NDEBUG" }

	configuration "gmake"
		buildoptions {"-std=gnu++11 -fPIC"}	
    
	configuration "macosx"	
		linkoptions {"-std=c++11 -stdlib=libc++ -fPIC /usr/local/lib/libprotobuf-lite.a"}	
		buildoptions {"-std=c++11 -stdlib=libc++ -fPIC"}	

---------------------------------------------------------------------------
