Symbolic link of protobuffer sources
ln -s /Users/username/protobuffer-2.5.0 protobuffer

Protocol buffers needs to be compiled and installed with fPIC
./configure CXXFLAGS="-fPIC"

compile lib on Mac OSX:
gcc -dynamiclib -o libsession.dylib -dy session.c

compile lib on Linux:
gcc -shared -o libsession.so -fPIC session.c

mv the library to the bin folder, then netty will automatically find it when running form eclipse
