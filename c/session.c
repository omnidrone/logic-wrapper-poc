#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct KeyStruct {
    const char* table;
    const char* id;
    const char* column;
} KeyStruct;

typedef struct DataStruct {
    KeyStruct key;
    const char* data;
} DataStruct;

const char* userIdPtr;

const char* (*loadDataPtr)(const KeyStruct);

void (*updateDataPtr)(const DataStruct);

void (*flushDataPtr)();

void (*sendMessagePtr)(const char*, int length);

    int init(const char* userId, const char* (*loadData)(const KeyStruct), void (*updateData)(const DataStruct), void (*flushData)(), void (*sendMessage)(const char*, int length))
    {
        printf("Init!\n");
        printf("userId: %s\n",userId);
	userIdPtr = userId;
        loadDataPtr = loadData;
        updateDataPtr = updateData;
        flushDataPtr = flushData;
        sendMessagePtr = sendMessage;
        fflush(stdout);
        return 0;
    }

    void onMessage(const char* userId, const char* proto, int protoLength)
    {
        printf("on message: %s\n", proto);
        printf("message length: %d\n", protoLength);
        printf("message is for user: %s\n", userId);
        fflush(stdout);
        struct KeyStruct key;
        key.table = "spaceship";
        key.id = "1";
        key.column = "document";
        const char* dataStruct = loadDataPtr(key);
        /*
        * KeyStruct*key = (KeyStruct*)malloc(sizeof(KeyStruct) * 1);
	* memset(*key, 0, sizeof(KeyStruct) * 1);
	* (*key)[0].table = "spaceship";
	* (*key)[0].id = "1";
	* (*key)[0].column = "document";
        */
        printf("Datastruct received!\n");

        printf("data length: %s\n", dataStruct);
        fflush(stdout);

        struct DataStruct updatedDataStruct;
        updatedDataStruct.key = key;
        updatedDataStruct.data = "{\"name\":\"falcon\",\"velocity\":876,\"cargo\":\"flies\"}";
        updateDataPtr(updatedDataStruct);
        
        printf("send message back to client!");
	const char* message = "abcdefghi";
        sendMessagePtr(message, 9);
        printf("message sent back to client");
        fflush(stdout);

         
  }


