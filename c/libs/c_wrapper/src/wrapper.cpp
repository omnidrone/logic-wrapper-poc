#include "session.h"

//#ifdef _WIN32
//#	define DLL_EXPORT __declspec(dllexport)
//#else
#	define DLL_EXPORT
//#endif

extern "C" DLL_EXPORT session* session_ctor(const char* userId);
session* session_ctor(const char* userId)
{
	return new session(userId);
}

extern "C" DLL_EXPORT int session_init(session* s, const char* (*get)(const char*, const char*), const char* (*getAttribute)(const char*, const char*, const char*), const char* (*getAttributes)(const char*, const char*, const char**, const int), void(*update)(const char*, const char*, const char*),void(*updateAttributes)(const char*, const char*, const char*), void(*flushData)(), void(*sendMessage)(const char*, int length))
{
	return s->init(get, getAttribute, getAttributes, update, updateAttributes, flushData, sendMessage);
}

extern "C" DLL_EXPORT void session_onMessage(session* s, const char* proto, int len)
{
	s->onMessage(proto, len);
}