#pragma once

#include <functional>
#include <map>

// Code used by JNA
typedef struct KeyStruct
{
	const char* table;
	const char* id;
	const char* column;
} KeyStruct;

typedef struct DataStruct
{
	KeyStruct key;
	const char* data;
} DataStruct;

using flushDataFunc = std::function < void() > ;
using sendMessageFunc = std::function < void(const char*, int len) > ;

using getFunc = std::function < const char*(const char*, const char*) > ;
using getAttributeFunc = std::function < const char*(const char*, const char*, const char*) > ;
using getAttributesFunc = std::function < const char*(const char*, const char*, const char**, const int) > ;
using updateFunc = std::function < void(const char*, const char*, const char*) > ;
using updateAttributesFunc = std::function < void(const char*, const char*, const char*) > ;


using userId = const char*;
class session;

class session
{
public:
	session(userId id);

public:
	int init(getFunc get, getAttributeFunc getAttribute, getAttributesFunc getAttributes, updateFunc update, updateAttributesFunc updateAttributes, flushDataFunc flush, sendMessageFunc sendMessage);
	void onMessage(const char* proto, int len);

private:
	userId id;

	getFunc get;
	getAttributeFunc getAttribute;
	getAttributesFunc getAttributes;
	updateFunc update;
	updateAttributesFunc updateAttributes;
	flushDataFunc flush;
	sendMessageFunc sendMessage;
};
