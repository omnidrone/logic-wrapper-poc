#include "session.h"
#include "logic.pb.h"

#include "format.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>
#include "flatbuffers/flatbuffers.h"
#include "flatbuffers/idl.h"
#include "flatbuffers/util.h"
#include "game_generated.h"
#include "order.cpp"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"
#include "avro/ValidSchema.hh"
#include "avro/Compiler.hh"
#include "avro/DataFile.hh"
#include <iomanip>

using namespace MyGame;
session::session(userId id)
	: id(id)
	, get(nullptr)
	, getAttribute(nullptr)
	, getAttributes(nullptr)
	, update(nullptr)
	, updateAttributes(nullptr)
	, flush(nullptr)
	, sendMessage(nullptr)
{
}

int session::init(getFunc get, getAttributeFunc getAttribute, getAttributesFunc getAttributes, updateFunc update, updateAttributesFunc updateAttributes, flushDataFunc flush, sendMessageFunc sendMessage)
{
	this->get = get;
	this->getAttribute = getAttribute;
	this->getAttributes = getAttributes;
	this->update = update;
	this->updateAttributes = updateAttributes;
	this->flush = flush;
	this->sendMessage = sendMessage;

  	return 0;
}

void session::onMessage(const char* data, int len)
{

	std::cout << id << std::endl;
	std::string tablename = "spaceship";
	// check if it's a command or a response
	logic::GameCommand command;
	if (command.ParseFromArray(data, len))
	{
		auto type = command.GetTypeName();
		std::cout << util::Format("GameCommand {0} received", type) << std::endl;
		std::cout << util::Format("GameCommand has GetSpaceship {0}", command.HasExtension(logic::getSpaceship)) << std::endl;
		std::cout << util::Format("GameCommand has GetSpaceships {0}", command.HasExtension(logic::getSpaceships)) << std::endl;
		std::cout << util::Format("GameCommand has UpdateCargo {0}", command.HasExtension(logic::updateCargo)) << std::endl;
		std::cout << util::Format("GameCommand has UpdateVelocity {0}", command.HasExtension(logic::updateVelocity)) << std::endl;
		std::cout << util::Format("GameCommand has CreateSpaceship {0}", command.HasExtension(logic::createSpaceship)) << std::endl;

		KeyStruct key;
		DataStruct up;
		if (command.HasExtension(logic::getSpaceship))
		{
			logic::GetSpaceship getSpaceship = command.GetExtension(logic::getSpaceship);
			

			std::string attribute = "spaceship";
			attribute += getSpaceship.id();

			//auto json = getAttribute(tablename.c_str(), id, attribute);

			std::vector<const char*> attributes;
			attributes.push_back(attribute.c_str());

			auto json = getAttributes(tablename.c_str(), id, attributes.data(), 1);
			std::cout << json << std::endl;

			rapidjson::Document spaceship;
			spaceship.Parse(json);
			//{"spaceshipad71":{"crewMembers":0,"name":"1st","id":"ad71","velocity":15,"cargo":"birch"}}
			logic::GameCommandResponse* commandResponseProto = new logic::GameCommandResponse();
			logic::GetSpaceshipResponse* spaceshipResponseProto = commandResponseProto->MutableExtension(logic::getSpaceshipResponse);//new logic::GetSpaceshipResponse();
			logic::Spaceship* spaceshipProto = spaceshipResponseProto->mutable_spaceship();
			if (spaceship.HasMember(attribute.c_str()))
			{
				spaceshipProto->set_id(spaceship[attribute.c_str()]["id"].GetString());
				spaceshipProto->set_name(spaceship[attribute.c_str()]["name"].GetString());
				spaceshipProto->set_velocity(spaceship[attribute.c_str()]["velocity"].GetInt());
				spaceshipProto->set_cargo(spaceship[attribute.c_str()]["cargo"].GetString());
			}
			std::string output;
			commandResponseProto->SerializeToString(&output);

			const char* c_output = output.c_str();
    		sendMessage(c_output, output.length());
		}
		else if (command.HasExtension(logic::updateVelocity))
		{
			logic::UpdateVelocity updateVelocity = command.GetExtension(logic::updateVelocity);
			

			std::string attribute = "spaceship";
			attribute += updateVelocity.id();
			
			std::string updatePath = attribute;
			updatePath += ".velocity";
			std::string velocity = std::to_string(updateVelocity.velocity());
			//specify update in json format
			rapidjson::Document d;
			d.SetObject();

			rapidjson::Value k;
			k.SetString(rapidjson::StringRef(updatePath.c_str()));
			rapidjson::Value v;
			v.SetInt(updateVelocity.velocity());
			d.AddMember(rapidjson::StringRef(updatePath.c_str()), v, d.GetAllocator());
			
			 // Stringify the DOM
    		rapidjson::StringBuffer buffer;
    		rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    		d.Accept(writer);
    		std::cout << buffer.GetString() << std::endl;
			updateAttributes(tablename.c_str(), id, buffer.GetString());	
		}
		else if (command.HasExtension(logic::updateCargo))
		{
			logic::UpdateCargo updateCargo = command.GetExtension(logic::updateCargo);
			

			std::string updatePath = "spaceship";
			updatePath += updateCargo.id();
			updatePath += ".cargo";

			//std::string updatePath = "spaceships.";
			//updatePath += updateCargo.id();
			//updatePath += ".cargo";

			//specify update in json format
			std::string json = "{\"" + updatePath + "\":\"" + updateCargo.cargo() + "\"}"; 
    		std::cout << json << std::endl;
			updateAttributes(tablename.c_str(), id, json.c_str());
		}
		else if (command.HasExtension(logic::getSpaceships))
		{
			logic::GetSpaceships getSpaceship = command.GetExtension(logic::getSpaceships);
			const char* result = get("spaceship", id);
			std::cout << result << std::endl;
			rapidjson::Document document;
			document.Parse(result);
			//{"spaceshipad71":{"crewMembers":0,"name":"1st","id":"ad71","velocity":15,"cargo":"birch"}}
			logic::GameCommandResponse* commandResponseProto = new logic::GameCommandResponse();
			logic::GetSpaceshipsResponse* spaceshipsResponseProto = commandResponseProto->MutableExtension(logic::getSpaceshipsResponse);
			
			for (rapidjson::Value::ConstMemberIterator itr = document.MemberBegin();
    			itr != document.MemberEnd(); ++itr)
			{
    			printf("member name is %s \n",itr->name.GetString());
				std::cout << itr->name.GetString() << std::endl;
				std::string s(itr->name.GetString());
				if (s.find("spaceship") == 0) 
				{
					std::cout << "its a spaceship!!" << std::endl;
					logic::Spaceship* spaceshipProto = spaceshipsResponseProto->add_spaceships();
					spaceshipProto->set_id(itr->value["id"].GetString());
					spaceshipProto->set_name(itr->value["name"].GetString());
					spaceshipProto->set_velocity(itr->value["velocity"].GetInt());
					spaceshipProto->set_cargo(itr->value["cargo"].GetString());
				}
			}

			
			std::string output;
			commandResponseProto->SerializeToString(&output);

			const char* c_output = output.c_str();
    		sendMessage(c_output, output.length());

		}
		else if (command.HasExtension(logic::createSpaceship))
		{
			logic::CreateSpaceship createSpaceship = command.GetExtension(logic::createSpaceship);
			int spaceshipId = rand();
			std::string attribute = "spaceship";
			attribute += std::to_string(spaceshipId);
			
			std::cout << "construct json" << std::endl;
			std::string json = "{\""+attribute+"\":{\"id\":\"";
			json += std::to_string(spaceshipId);
			json += "\",\"name\":\"";
			json += createSpaceship.name();
			json += "\",\"velocity\":";
			json += std::to_string(createSpaceship.velocity());
			json +=",\"cargo\":\"";
			json += createSpaceship.cargo();
			json +="\"}}"; 

			/*
			std::string json = "{\"spaceships\":{\"";
			json += std::to_string(spaceshipId);
			json +="\":{\"name\":\"";
			json += createSpaceship.name();
			json += "\",\"velocity\":";
			json += std::to_string(createSpaceship.velocity());
			json +=",\"cargo\":\"";
			json += createSpaceship.cargo();
			json +="\"}}}"; 
			*/
			std::cout << json << std::endl;
			updateAttributes(tablename.c_str(), id, json.c_str());
			//update2(tablename.c_str(), id, json.c_str());
		}
		else
		{
			std::cout << util::Format("Unknown command {0} received", command.GetTypeName()) << std::endl;
		}
	}
	else
	{
		// it's not a command, check if its a response
		logic::GameCommandResponse response;
		if (response.ParseFromArray(data, len))
		{
			logic::GetSpaceshipResponse spaceshipResponse;
			spaceshipResponse.ParseFromArray(data, len);

			logic::Spaceship spaceship = spaceshipResponse.spaceship();
			std::cout << util::Format("Spaceship {0} - {1} - {2}", spaceship.name(), spaceship.velocity(), spaceship.cargo()) << std::endl;
		}
		else
		{
			std::cout << "Error parsing the message" << std::endl;
		}
	}

}

