#include "session.h"
#include "logic.pb.h"

#include "format.h"

//#include "rapidjson/document.h"
//#include "rapidjson/writer.h"
//#include "rapidjson/stringbuffer.h"
//#include <iostream>

session::session(userId id)
	: id(id)
	, get(nullptr)
	, getAttributes(nullptr)
	, update(nullptr)
	, update2(nullptr)
	, updateAttributes(nullptr)
	, flush(nullptr)
	, send(nullptr)
{
}

int session::init(getFun get, getAttributesFun getAttributes, updateFun update, update2Fun update2, updateAttributesFun updateAttributes, flushDataFun flush, sendMessageFun send)
{
	this->get = get;
	this->getAttributes = getAttributes;
	this->update = update;
	this->update2 = update2;
	this->updateAttributes = updateAttributes;
	this->flush = flush;
	this->send = send;

	return 0;
}

void session::onMessage(const char* data, int len)
{
	// check if it's a command or a response
	logic::GameCommand command;
	if (command.ParseFromArray(data, len))
	{
		auto type = command.GetTypeName();
		std::cout << util::Format("GameCommand {0} received", type) << std::endl;
		std::cout << util::Format("GameCommand has GetSpaceship {0}", command.HasExtension(logic::getSpaceship)) << std::endl;
		std::cout << util::Format("GameCommand has GetSpaceships {0}", command.HasExtension(logic::getSpaceships)) << std::endl;
		std::cout << util::Format("GameCommand has UpdateCargo {0}", command.HasExtension(logic::updateCargo)) << std::endl;
		std::cout << util::Format("GameCommand has UpdateVelocity {0}", command.HasExtension(logic::updateVelocity)) << std::endl;
		std::cout << util::Format("GameCommand has CreateSpaceship {0}", command.HasExtension(logic::createSpaceship)) << std::endl;

		KeyStruct key;
		DataStruct up;
		if (type == "logic.GetSpaceship")
		{
			key.table = "spaceship";
			key.id = "1";
			key.column = "document";
			logic::GetSpaceship getSpaceship = command.GetExtension(logic::getSpaceship);
			char** attributes = new char*[1];
			attributes[0] = "spaceship"+getAttributes.getId();
			char* json = getAttributes("spaceship", id, attributes);
			std::cout << json << std:endl;
		}
		else if (type == "logic.UpdateVelocity")
		{
			up.key = key;
			up.data = "{\"velocity\": 123}";
			updateDataFun(up);
		}
		else if (type == "logic.UpdateCargo")
		{
			up.key = key;
			up.data = "{\"cargo\": things}";
			updateDataFun(up);
		}
	}
	else
	{
		// it's not a command, check if its a response
		logic::GameCommandResponse response;
		if (response.ParseFromArray(data, len))
		{
			logic::GetSpaceshipResponse spaceshipResponse;
			spaceshipResponse.ParseFromArray(data, len);

			logic::Spaceship spaceship = spaceshipResponse.spaceship();
			std::cout << util::Format("Spaceship {0} - {1} - {2}", spaceship.name(), spaceship.velocity(), spaceship.cargo()) << std::endl;
		}
		else
		{
			std::cout << "Error parsing the message" << std::endl;
		}
	}
}
