#include <iostream>
#include <memory>
#include <string>

#include <grpc++/grpc++.h>

#include "service-db.grpc.pb.h"
#include "service-db.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using service_db::GetRequest;
using service_db::GetResponse;
using service_db::UpdateRequest;
using service_db::UpdateResponse;
using service_db::DatabaseRPC;

class DbClient {
 public:
  DbClient(std::shared_ptr<Channel> channel)
      : stub_(DatabaseRPC::NewStub(channel)) {}

  // Assambles the client's payload, sends it and presents the response back
  // from the server.
  std::string Get(const std::string& tableId, const std::string& id) {
    // Data we are sending to the server.
    GetRequest request;
    request.set_tableid(tableId);
    request.set_id(id);

    // Container for the data we expect from the server.
    GetResponse response;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->Get(&context, request, &response);

    // Act upon its status.
    if (status.ok()) {
      return response.json();
    } else {
      return "RPC failed";
    }
  }

 private:
  std::unique_ptr<DatabaseRPC::Stub> stub_;
};

