#include <iostream>
#include <memory>
#include <string>
#include <grpc++/grpc++.h>

#include "service-cpp.grpc.pb.h"
#include "service-db.grpc.pb.h"
#include "DbClient.h"

//server grpc
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

//client grpc
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

using service_cpp::InitRequest;
using service_cpp::InitResponse;
using service_cpp::GameRequest;
using service_cpp::GameResponse;
using service_cpp::CppRPC;

using service_db::GetRequest;
using service_db::GetResponse;
using service_db::UpdateRequest;
using service_db::UpdateResponse;
using service_db::DatabaseRPC;

// Logic and data behind the server's behavior.
class ServerImpl final : public CppRPC::Service {

   
  Status Init(ServerContext* context, const InitRequest* request,
                  InitResponse* reply) override {
    std::cout << "Init received" << std::endl;
    std::string userId = request->userid();
    std::cout << userId << std::endl;
    //std::string prefix("Hello ");
    reply->set_success(true);
    //reply->set_message(prefix + request->name());
    return Status::OK;
  }
  Status OnMessage(ServerContext* context, const GameRequest* request,
                  GameResponse* reply) override {
    std::cout << "OnMessage received" << std::endl;
    std::cout << request->userid() << std::endl;
    game::GameCommand command = request->gamecommand();
    google::protobuf::Any any = command.command();
    std::cout << any.mutable_type_url() << std::endl;

    DbClient dbClient(
      grpc::CreateChannel("localhost:50052", grpc::InsecureChannelCredentials()));
    /*
    std::cout << util::Format("GameCommand has GetSpaceship {0}", command.HasExtension(game::getSpaceship)) << std::endl;
    std::cout << util::Format("GameCommand has GetSpaceships {0}", command.HasExtension(game::getSpaceships)) << std::endl;
    std::cout << util::Format("GameCommand has UpdateCargo {0}", command.HasExtension(game::updateCargo)) << std::endl;
    std::cout << util::Format("GameCommand has UpdateVelocity {0}", command.HasExtension(game::updateVelocity)) << std::endl;
    std::cout << util::Format("GameCommand has CreateSpaceship {0}", command.HasExtension(game::createSpaceship)) << std::endl;
    */
    game::GameCommandResponse *gameCommandResponse = new game::GameCommandResponse();
    std::cout << any.value() << std::endl;
    if (any.Is<game::GetSpaceship>())
    {

      std::string table("spaceship");
      std::string id("1");
      std::string response = dbClient.Get(table, id);
      std::cout << "Greeter received: " << reply << std::endl;

      game::GetSpaceshipResponse spaceshipResponseProto; //= new game::GetSpaceshipResponse();//gameCommandResponse->set_command(game::GetSpaceshipResponse);//new logic::GetSpaceshipResponse();
      game::Spaceship* spaceshipProto = spaceshipResponseProto.mutable_spaceship();
      //if (spaceship.HasMember(attribute.c_str()))
      //{
        spaceshipProto->set_id("spaceship id");//spaceship[attribute.c_str()]["id"].GetString());
        spaceshipProto->set_name("falcon");//spaceship[attribute.c_str()]["name"].GetString());
        spaceshipProto->set_velocity(666);//spaceship[attribute.c_str()]["velocity"].GetInt());
        spaceshipProto->set_cargo("metal");//spaceship[attribute.c_str()]["cargo"].GetString());
      //}
      gameCommandResponse->mutable_command()->PackFrom(spaceshipResponseProto);

    } 
    else if (command.command().Is<game::CreateSpaceship>())
    {
      game::CreateSpaceship createSpaceship;// = new game::CreateSpaceship();//command.GetExtension(game::createSpaceship);
      int spaceshipId = rand();
      std::string attribute = "spaceship";
      attribute += std::to_string(spaceshipId);
      
      std::cout << "construct json" << std::endl;
      std::string json = "{\""+attribute+"\":{\"id\":\"";
      json += std::to_string(spaceshipId);
      json += "\",\"name\":\"";
      json += createSpaceship.name();
      json += "\",\"velocity\":";
      json += std::to_string(createSpaceship.velocity());
      json +=",\"cargo\":\"";
      json += createSpaceship.cargo();
      json +="\"}}"; 

      /*
      std::string json = "{\"spaceships\":{\"";
      json += std::to_string(spaceshipId);
      json +="\":{\"name\":\"";
      json += createSpaceship.name();
      json += "\",\"velocity\":";
      json += std::to_string(createSpaceship.velocity());
      json +=",\"cargo\":\"";
      json += createSpaceship.cargo();
      json +="\"}}}"; 
      */
      std::cout << json << std::endl;
      //updateAttributes(tablename.c_str(), id, json.c_str());
      //update2(tablename.c_str(), id, json.c_str());
    }
    else
    {
      std::cout << "Unknown command received" << std::endl;
    }
    reply->set_allocated_gamecommandresponse(gameCommandResponse);
    //reply->set_message(prefix + request->name());
    return Status::OK;
  }
};

void RunServer() {
  std::string server_address("0.0.0.0:50051");
  ServerImpl service;

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

int main(int argc, char** argv) {
  RunServer();

  return 0;
}
